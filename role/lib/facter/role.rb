Facter.add(:role) do
  setcode do
    if /^(?<role>\w+)-\w/ =~ Facter.value(:networking)['hostname']
      role
    end
  end
end
