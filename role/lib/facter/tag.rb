Facter.add(:tag) do
  setcode do
    if /^\w+-(?<tag>\w+)/ =~ Facter.value(:networking)['hostname']
      tag
    end
  end
end
