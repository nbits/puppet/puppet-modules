# Manage a webserver
class role::webserver (
  Hash $websites  = {},
  Hash $locations = {},
) {
  include profile::firewall::webserver
  include profile::letsencrypt
  include profile::nginx
  include profile::nextcloud
  include profile::wordpress

  $websites.each |$key, $val| {
    profile::website { $key:
      www_root   => $val['www_root'],
      ssl        => $val['ssl'],
      add_header => $val['add_header'],
    }
  }

  $locations.each |$key, $val| {
    nginx::resource::location { $key:
      www_root      => $val['www_root'],
      server        => $val['server'],
      location      => $val['location'],
      fastcgi       => $val['fastcgi'],
      fastcgi_param => $val['fastcgi_param'],
      try_files     => $val['try_files'],
    }
  }

  include php

  Profile::Reverse_proxy <<| |>>

}
