# Configure ONLYOFFICE using Hiera:
#
#     role::onlyoffice::instances:
#       instance_name:
#          uid: 5000000
#          gid: 5000000
#          jwt_secret: very-secretive-passphrase
#          local_port: 8000
#          public_port: 443
#          public_address: onlyoffice.example.org
class role::onlyoffice (
  Hash $instances = {},
) {

  ## Podman registry mirror configuration

  include profile::registry_proxy

  $registries_conf = '/etc/containers/registries.conf'
  $mirror = {
    location => '127.0.0.1:5000',
    insecure => true,
  }

  file { $registries_conf:
    ensure  => file,
    mode    => '0644',
    content => epp('profile/podman/registries.conf.epp', {
      mirror => $mirror,
    }),
    require => [
      Class['profile::registry_proxy'],
      Package['podman'],
    ]
  }

  ## ONLYOFFICE profile configuration

  class { 'profile::onlyoffice':
    instances => $instances,
    require   => [
      Class['profile::registry_proxy'],
      File[$registries_conf],
    ],
  }

}
