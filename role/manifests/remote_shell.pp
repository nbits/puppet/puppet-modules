# A simple remote shell
class role::remote_shell {
  include profile::weechat
  include profile::mosh
}
