# A Puppet Server
class role::puppetserver {

  include profile::sshd

  # Class `profile::puppet` is added by `profile::base::minimal`, which is
  # included in all nodes. Also, `profile::puppetserver::type` is set to
  # `'server'` by Hiera. So we don't really need to include any Puppet here.

}
