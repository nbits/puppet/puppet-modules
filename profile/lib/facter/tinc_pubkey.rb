Facter.add(:tinc_pubkey) do
  setcode do
    begin
      File.read('/etc/tinc/tun0/rsa_key.pub')
    rescue Errno::ENOENT
      ''
    end
  end
end
