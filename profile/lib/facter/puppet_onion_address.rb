Facter.add(:puppet_onion_address) do
  setcode do
    begin
      File.read('/var/lib/tor/puppet/hostname').strip
    rescue Errno::ENOENT
      ''
    end
  end
end
