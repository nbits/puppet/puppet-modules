#!/bin/bash
#
# Puppet External Node Classifier
# -------------------------------
#
# This script parses FQDNs in the following format:
#
#     ROLE-TAG[-ENV].DOMAIN
#
# And returns the proper classes and environment accordingly:
#
#     - Class role::ROLE, if that role actually exists in the manifests tree
#       for the respective environment.
#
#     - ENV is optional: if it is a prefix of one of "production", "staging" or
#       "development", then one of these is returned. If not, "production" is
#       returned.
#
# The TAG is not relevant for this script, and should be used to make the
# hostname unique and possibly easily identifiable (eg. 'myinfra_f29bd8c9').
#
# All values should be composed of alpha-numeric and '_' characters.
#
# If a FQDN doesn't match as above, no special information is returned.
#
# See: https://puppet.com/docs/puppet/5.5/nodes_external.html

if [ ${#} -ne 1 ]; then
	echo "Usage: ${0} NODE"
	exit 1
fi

DEFAULT_ENV='production'
ENVIRONMENTS='production staging development'

get_environment() {

	# Return one of the ENVIRONMENTS defined above if ENV is a prefix of
	# one of them, otherwise return "production".

	prefix=${1}
	environment=""
	for env in ${ENVIRONMENTS}; do
		if [[ ${env} == ${prefix}* ]]; then
			environment=${env}
			break
		fi
	done
       if [ -z "${environment}" ]; then
               environment="${DEFAULT_ENV}"
       fi
       echo ${environment}
}

enc() {

	# Test whether the given FQDN matches ROLE-TAG[-ENV].DOMAIN, and act
	# accordingly.

	#       1. role         2. tag           4. environment
	regex="^([[:alnum:]_]+)-([[:alnum:]_]+)(-([[:alnum:]_]+))?((\.[[:alnum:]_-]+)?)+$"
	fqdn=${1}

	# XXX Exclude hosts starting with "gitlab-": we don't have such a role
	#     but need to deal with hostnames we don't currently control (eg.
	#     in GitLab CI nodes' hostnames start with "gitlab-runner...").
	if [[ ${fqdn} == gitlab-* ]]; then
		echo "classes:"
		exit 0
	fi

	# we're only interested in matching FQDNs that match the regular expression
	if [[ ! ${fqdn} =~ ${regex} ]]; then
		echo "classes:"
		exit 0
	fi

	environment=$( get_environment ${BASH_REMATCH[4]} )
	role=${BASH_REMATCH[1]}

	echo "classes:"
	if [ -n "${role}" ]; then
		echo "  - profile::base::minimal"
		echo "  - role::${role}"
	fi
	echo "environment: ${environment}"
}

run_tests() {

	# If FQDN = ROLE-TAG[-ENV].DOMAIN, we expect `enc` to output:
	#
	#   ----------8<----------
	#   classes:
	#     - role::${ROLE}
	#   environment: ${REAL_ENV}
	#   ----------8<----------
	#
	# where REAL_ENV is either one of ENVIRONMENTS as defined above (if
	# ENV is a prefix of one of them) or just ENV otherwise.
        #
	# If FQDN does not fit the model, we expect just an empty `classes`
	# hash:
	#
	#   ----------8<----------
	#   classes:
	#   ----------8<----------

	set -ex

	# Test uses of environment prefixes.

	output=$( enc server-some_tag-prod.example.com )
        [ "${output}" == $'classes:\n  - profile::base::minimal\n  - role::server\nenvironment: production' ] || exit 1

	output=$( enc puppetserver-some_tag-stag.example.com )
        [ "${output}" == $'classes:\n  - profile::base::minimal\n  - role::puppetserver\nenvironment: staging' ] || exit 1

	output=$( enc webserver-some_tag-dev.example.com )
        [ "${output}" == $'classes:\n  - profile::base::minimal\n  - role::webserver\nenvironment: development' ] || exit 1

	# Test uses of full environment names.

	output=$( enc server-some_tag-production.example.com )
        [ "${output}" == $'classes:\n  - profile::base::minimal\n  - role::server\nenvironment: production' ] || exit 1

	output=$( enc puppetserver-some_tag-staging.example.com )
        [ "${output}" == $'classes:\n  - profile::base::minimal\n  - role::puppetserver\nenvironment: staging' ] || exit 1

	output=$( enc webserver-some_tag-development.example.com )
        [ "${output}" == $'classes:\n  - profile::base::minimal\n  - role::webserver\nenvironment: development' ] || exit 1

	# Test using an arbitrary environment

	output=$( enc server-some_tag-someenv.example.com )
        [ "${output}" == $'classes:\n  - profile::base::minimal\n  - role::server\nenvironment: production' ] || exit 1

	# Test default production environment when not present in hostname

	output=$( enc server-some_tag.example.com )
        [ "${output}" == $'classes:\n  - profile::base::minimal\n  - role::server\nenvironment: production' ] || exit 1

	# Test using something that doesn't match ROLE-TAG[-ENV].DOMAIN

	output=$( enc arbitraryfqdn.example.com )
        [ "${output}" == $'classes:' ] || exit 1

	# Test using subdomains of roles

	output=$( enc arbitraryfqdn.server-1 )
        [ "${output}" == $'classes:' ] || exit 1

	# Test using role-subdomains of roles

	output=$( enc monitoring-1.server-1 )
        [ "${output}" == $'classes:\n  - profile::base::minimal\n  - role::monitoring\nenvironment: production' ] || exit 1

	# Make sure hosts starting with "gitlab-" are ignored

	output=$( enc gitlab-mytag-3.example.com )
        [ "${output}" == $'classes:' ] || exit 1

}

if [ ${1} == '--run-tests' ]; then
	run_tests
else
	enc ${1}
fi
