#!/bin/sh
#
# This scripts completely deletes a Libvirt guest by doing the following:
#
#   - Ask for confirmation for such controversial action.
#   - Destroy the guest if it is running.
#   - Undefine the guest in libvirt.
#   - Delete guest's LVM Logical Volume.

set -ex

GUEST=$1

if [ ${#} -ne 1 ]; then
        echo "Usage: ${0} GUEST"
        exit 1
fi

echo -n "Are you sure you want to delete VM ${GUEST}? Type VM name to confirm: "
read confirmation

if [ "${confirmation}" != "${GUEST}" ]; then
        echo "Error: deletion not confirmed, giving up!"
        exit 1
fi

( virsh list | grep ${GUEST} ) && virsh destroy ${GUEST}
[ -f /etc/libvirt/qemu/${GUEST}.xml ] && virsh undefine ${GUEST}
[ -b /dev/default/${GUEST} ] && lvremove -f /dev/default/${GUEST}
