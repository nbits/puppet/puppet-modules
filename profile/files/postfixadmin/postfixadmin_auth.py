#!/usr/bin/env python3
#
# postfixadmin_auth.py
#
# Provide a Nginx-compatible HTTP-based auth server that queries credentials
# from a Postfixadmin MySQL database.
#
# See: http://nginx.org/en/docs/mail/ngx_mail_auth_http_module.html#protocol
#
# Database configuration is read from a file:
#
#     # /etc/postfixadmin-auth.ini
#     [postfixadmin-auth]
#     user = [username]
#     host =  [hostname]
#     passwd =  [password]
#     db =  [database]

import configparser
import crypt
import logging
import MySQLdb
import socket

from dovecotauth import connect
from flask import Flask, request, make_response


LOG_FORMAT = '%(levelname)s %(message)s'
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)


AUTH_SERVER = '192.168.122.224'
REQUIRED_HEADERS = [
    'Auth-Method',
    'Auth-User',
    'Auth-Pass',
    'Auth-Protocol',
    'Auth-Login-Attempt',
]
CONFIG_FILE = '/etc/postfixadmin-auth.ini'
PORTS = {
    'imap': 143,
    'pop3': 110,
    'smtp': 587,
}


app = Flask(__name__)


def _invalid(resp):
    resp.headers['Auth-Status'] = 'Invalid login or password'
    resp.headers['Auth-Wait'] = '3'
    return resp


def _check_creds(user, pw):
    with connect('PLAIN', unix='/var/spool/postfix/private/auth') as conn:
        status, flags = conn.auth('PLAIN', user, pw)
        return status


@app.route("/")
def mailbox_auth():
    resp = make_response('', 200)

    if set(REQUIRED_HEADERS).issubset(set(request.headers.keys())):
        user = request.headers.get('Auth-User')
        pw = request.headers.get('Auth-Pass')
        proto = request.headers.get('Auth-Protocol')
        client_ip = request.headers.get('Client-Ip')
        if proto in PORTS and _check_creds(user, pw):
            app.logger.info(f'{client_ip} {proto} {user} ok')
            resp.headers.set('Auth-Status', 'OK')
            resp.headers.set('Auth-Server', AUTH_SERVER)
            resp.headers.set('Auth-Port', str(PORTS[proto]))
            return resp
        else:
            app.logger.info(f'{client_ip} {proto} {user} invalid')
            return _invalid(resp)

    app.logger.error(f'malformed request')
    return _invalid(resp)


if __name__ == '__main__':
    app.run()
