/* Execute the `check_borg_backup` command as setuid.
 *
 * Monitoring of Borg backups depends on having the repository passphrase in
 * order to get a list of Borg archives.
 *
 * Because our Borg check command is written in a scripting language and
 * because setuid doesn't work with scripts in Linux, we need a setuid binary
 * that can run the script with the appropriate privileges.
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define COMMAND "/usr/local/bin/check_borg_backup"
#define UID 0
#define ERROR_STATUS 3  // Should correspond to an `Unknown` status in Icinga2

int main(int argc, char** argv)
{
        if (setuid(UID) != 0) {
                printf("Set UID error: %s\n", strerror(errno));
                return ERROR_STATUS;
        }
        if (execve(COMMAND, argv, NULL) != 0){
                printf("Exec error: %s\n", strerror(errno));
                return ERROR_STATUS;
        }
}
