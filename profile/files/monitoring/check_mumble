#!/usr/bin/env python3

# Based in: https://wiki.mumble.info/wiki/Protocol

import argparse
import os
import socket
import struct
import sys


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('host', help='the mumble host to check')
    parser.add_argument('port', type=int, nargs='?', default=64738, help='the port where mumble is listening')
    args = parser.parse_args()
    return args


def main(host, port):
    sent_id = struct.unpack('q', os.urandom(8))[0]
    data = struct.pack(">iq", 0, sent_id)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(5)

    try:
        sock.sendto(data, (host, port))
        response = sock.recv(24)
        version, recv_id, users, max, bw = struct.unpack(">iqiii", response)
        assert sent_id == recv_id, "sent and received ids don't match"
        output = "Version: {}".format(version)
        output += ", Current users: {}".format(users)
        output += ", Max users: {}".format(max)
        output += ", Max bw: {}".format(bw)
        print(output)
    except Exception as e:
        print("Error: {}".format(str(e)))
        sys.exit(2)


if __name__ == "__main__":
    args = parse_args()
    main(args.host, args.port)
