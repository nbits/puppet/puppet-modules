# Install Docker
class profile::docker (
  Hash $instance = {},
  Array $extra_parameters = [],
) {
  class { 'docker':
    extra_parameters => $extra_parameters,
  }

  class { 'docker::run_instance':
    instance => $instance,
  }
}
