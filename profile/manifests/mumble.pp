# Install Mumble
class profile::mumble {

  ensure_packages(['mumble-server'])

  service { 'mumble-server':
    ensure => 'running',
  }

  include profile::mumble::firewall

}
