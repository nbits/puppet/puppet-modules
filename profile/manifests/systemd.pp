# A simple systemd profile
class profile::systemd (
  Array $dns = ['208.67.222.222', '208.67.220.220'],
) {

  apt::pin { 'systemd from backports':
    ensure   => absent,
    packages => [ 'systemd', 'libsystemd0' ],
    release  => 'bullseye-backports',
    priority => 500,
  }

  class { 'systemd':
    manage_resolved  => true,
    manage_timesyncd => true,
    dns              => $dns.join(' '),
  }

}
