# @summary
#   Manage user accounts.
#
# @example Basic usage
#   class { 'profile::accounts':
#     users => {
#       '.*' => {
#         bob => {
#           uid      => '4001',
#           gid      => '4001',
#           group    => 'staff',
#           shell    => '/bin/bash',
#           password => '!!',
#           locked   => false,
#         },
#       },
#       '^example\.org$' => {
#         alice => { password => '123' },
#       },
#     }
#   }
#
# @param users
#   A Hash whose indexes are matched against $trusted['certname'] to create a
#   list of users for that node.
#
class profile::accounts (
  Hash[String, Hash] $users = {},
) {
  ensure_packages(['fish'])

  include accounts

  $users.each | String $domain_regexp, Hash $domain_users | {
    if $trusted['certname'] =~ Regexp($domain_regexp) {
      ensure_resources('accounts::user', $domain_users)  # TODO: purge SSH keys
    }
  }

  include sudo

  sudo::conf { 'sudo':
    priority => 10,
    content  => '%sudo ALL=(ALL) NOPASSWD: ALL',
  }
}
