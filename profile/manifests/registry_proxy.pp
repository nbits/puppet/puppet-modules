# A container registry that mirrors docker.io
class profile::registry_proxy (
  String $upstream = 'https://registry-1.docker.io',
) {

  $config_yaml = '/etc/docker/registry/config.yml'

  package { 'docker-registry':
    ensure => installed,
  }

  service { 'docker-registry':
    ensure  => running,
    require => Package['docker-registry'],
  }

  file { $config_yaml:
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => epp('profile/registry/config.yml.epp', {
      proxy => $upstream,
    }),
    require => Package['docker-registry'],
    notify  => Service['docker-registry'],
  }

}
