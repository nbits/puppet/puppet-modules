# This class is meant to be used as a way to bootstrap the use of the
# `profile::base` class.
class profile::base::minimal (
  Boolean $manage_firewall = true,
  Boolean $manage_mail = true,
) {

  include profile::accounts
  include profile::tinc
  include profile::puppet
  include profile::monitoring
  include profile::sshd
  include profile::apt
  include profile::systemd
  include profile::packages
  include profile::needrestart

  if $manage_firewall {
    include profile::firewall
  }

  if $manage_mail {
    include profile::mail
  }

  Class['profile::tinc'] -> Class['profile::puppet']
  Class['profile::tinc'] -> Class['profile::monitoring']

}
