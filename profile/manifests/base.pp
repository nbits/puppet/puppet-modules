# Base profile included by all nodes
class profile::base {
  include profile::base::minimal
  include profile::network
}
