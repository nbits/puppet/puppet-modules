# An entrypoint for GitLab Runner definitions
define profile::gitlab_runner::runner (
  String $url,
  String $token,
  Enum['docker', 'libvirt'] $type,
  Boolean $use_host_docker = true,
  Optional[Integer] $output_limit = undef,
) {
  if $type == 'docker' {
    profile::gitlab_runner::docker { $name:
      url             => $url,
      token           => $token,
      use_host_docker => true,
      output_limit    => $output_limit,
    }
  } elsif $type == 'libvirt' {
    profile::gitlab_runner::libvirt { $name:
      url          => $url,
      token        => $token,
      output_limit => $output_limit,
    }
  }
}
