# Create a Libvirt-based GitLab Runner
define profile::gitlab_runner::libvirt (
  String $url,
  String $token,
  Optional[Integer] $output_limit = undef,
) {

  concat::fragment { "config.toml Libvirt runner ${name}":
    target  => '/etc/gitlab-runner/config.toml',
    content => epp('profile/gitlab_runner/config.toml.libvirt.epp', {
      name        => $name,
      url         => $url,
      token       => $token,
      output_limt => $output_limit,
    }),
    order   => 2,
    notify  => Service['gitlab-runner'],
  }

}
