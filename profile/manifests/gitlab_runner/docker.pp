# Create a Docker-based GitLab Runner
define profile::gitlab_runner::docker (
  String $url,
  String $token,
  Boolean $use_host_docker = true,
  Optional[Integer] $output_limit = undef,
) {

  concat::fragment { "config.toml Docker runner ${name}":
    target  => '/etc/gitlab-runner/config.toml',
    content => epp('profile/gitlab_runner/config.toml.docker.epp', {
      name            => $name,
      url             => $url,
      token           => $token,
      use_host_docker => $use_host_docker,
      output_limit    => $output_limit,
    }),
    order   => 2,
    notify  => Service['gitlab-runner'],
  }

}
