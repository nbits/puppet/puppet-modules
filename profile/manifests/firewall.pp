# Systems firewall
class profile::firewall (
  Boolean $purge = true,
  Array $input_filter_ignore = [],
  Array[String] $subclasses = [],
  Hash $rules = {},
) {

  resources { 'firewall':
    purge => $purge,
  }

  class { 'profile::firewall::pre': }
  class { 'profile::firewall::post': }

  Firewall {
    before  => Class['profile::firewall::post'],
    require => Class['profile::firewall::pre'],
  }

  include firewall

  firewallchain { 'INPUT:filter:IPv4':
    ensure => present,
    purge  => $purge,
    ignore => $input_filter_ignore,
    policy => drop,
    before => undef,
  }

  firewallchain { 'FORWARD:filter:IPv4':
    ensure => present,
    policy => drop,
    before => undef,
  }

  firewallchain { 'OUTPUT:filter:IPv4':
    ensure => present,
    policy => accept,
    before => undef,
  }

  $subclasses.map | $subclass | { "profile::firewall::${subclass}"  }.include
  create_resources(firewall, $rules)

}
