# An APT profile that sets a Debian repository for the current release by
# default.
class profile::apt (
  Boolean $auto_reboot = false,
  String $release = $facts['os']['distro']['codename'],
  Hash $sources = {},
  Hash $purge = {},
  Hash $pins = {},
) {

  class { 'apt':
    sources => $sources,
    purge   => $purge,
    pins    => $pins,
  }

  # When taking over configuration of APT sources, make sure to provide a
  # minimally secure set of repositories.
  if $purge['sources.list'] and $purge['sources.list.d'] {

    apt::source { $release:
      location => 'http://deb.debian.org/debian',
      repos    => 'main contrib',
    }

    if $release =~ /^(buster|bullseye|bookworm)$/ {
      $security_release = $release ? {
        'buster' => "${release}/updates",
        default  =>"${release}-security",
      }
      apt::source { "security.debian.org-${release}-security":
        release  => $security_release,
        location => 'http://security.debian.org/debian-security',
        repos    => 'main contrib',
      }
    } else {
      notify { 'no-debian-security-repo':
        message => 'No debian-security repository configured.',
      }
    }

  }

  if $release =~ /^(buster|bullseye)$/ {
    include apt::backports
  }

  class { 'unattended_upgrades':
    auto => { 'reboot' => $auto_reboot },
  }

}
