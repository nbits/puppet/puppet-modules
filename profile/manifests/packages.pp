# Install system packages
class profile::packages (
  Array[String] $ensure_installed = [],
  Array[String] $ensure_absent = [],
) {
  ensure_packages($ensure_installed)
  ensure_packages($ensure_absent, { ensure => absent })
}
