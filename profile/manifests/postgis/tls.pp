# TLS support for PostGIS
class profile::postgis::tls (
  String $domain = $::fqdn,
  Stdlib::IP::Address::V4::Nosubnet $listen_address = '127.0.0.1',
  Stdlib::Absolutepath $ssl_cert_file = '/var/lib/postgresql/tls/fullchain.pem',
  Stdlib::Absolutepath $ssl_key_file = '/var/lib/postgresql/tls/privkey.pem',
) {

  include profile::nginx
  include profile::letsencrypt

  nginx::resource::server { $domain: }

  $acme_webroot = '/var/www/acme'

  firewall { '100 accept HTTP on port 80':
    dport       => [ 80 ],
    proto       => 'tcp',
    action      => 'accept',
    destination => "${listen_address}/32",
  }

  -> nginx::resource::location { "acme-webroot-${domain}":
    server               => $domain,
    location             => '^~ /.well-known/acme-challenge/',
    www_root             => $acme_webroot,
    try_files            => [ '$uri =404' ],
    index_files          => [],
    location_cfg_prepend => { 'default_type' => 'text/plain' },
  }

  -> file { '/var/lib/postgresql/tls':
    ensure => directory,
    owner  => 'postgres',
    group  => 'postgres',
    mode   => '0750',
  }

  -> profile::letsencrypt::certonly { $domain: }

  ~> exec { "Copy TLS cert for ${domain} to a place available to PostgreSQL":
    command     => "/usr/bin/cp /etc/letsencrypt/live/${domain}/privkey.pem /etc/letsencrypt/live/${domain}/fullchain.pem /var/lib/postgresql/tls/",  # lint:ignore:140chars -- command
    refreshonly => true,
  }

  -> file { $ssl_cert_file:
    ensure => file,
    owner  => 'postgres',
    group  => 'postgres',
    mode   => '0644',
  }

  -> file { $ssl_key_file:
    ensure => file,
    owner  => 'postgres',
    group  => 'postgres',
    mode   => '0600',
  }

}
