# Install Gitolite3
class profile::gitolite3 {

  stdlib::ensure_packages( ['gitolite3'] )

  $basedir = '/var/lib/gitolite3'

  user { 'gitolite3':
    ensure     => present,
    require    => Package['gitolite3'],
    home       => $basedir,
    managehome => false,
  }

  group { 'gitolite3':
    ensure  => present,
    require => Package['gitolite3'],
  }

  file { $basedir:
    ensure  => directory,
    owner   => 'gitolite3',
    group   => 'gitolite3',
    mode    => '0755',
    require => [
      User['gitolite3'],
      Group['gitolite3'],
    ],
  }

  [ '/etc/gitolite3/gitolite.rc', '/var/lib/gitolite3/.gitolite.rc' ].each | String $path | {
    file { $path:
      ensure  => present,
      source  => 'puppet:///modules/profile/etc/gitolite3/gitolite.rc',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => [
        Package['gitolite3'],
        File[$basedir],
      ],
    }
  }

}
