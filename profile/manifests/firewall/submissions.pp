# Firewall for submissions port
class profile::firewall::submissions () {

  firewall { '100 allow TCP access on submissions port':
    dport  => [ 465 ],
    proto  => 'tcp',
    action => 'accept',
  }

}
