# Firewall for IMAPS
class profile::firewall::imaps () {

  firewall { '100 allow TCP access on IMAPS port':
    dport  => [ 993 ],
    proto  => 'tcp',
    action => 'accept',
  }

}
