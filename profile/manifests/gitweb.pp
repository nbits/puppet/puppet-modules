# Install Gitweb
class profile::gitweb (
  String $domain = "git.${::domain}",
  Stdlib::Absolutepath $projectroot = '/var/lib/git',
  Boolean $ssl = true,
) {

  # Apache2 is recommended by Gitweb in Debian, but we want to use Nginx as
  # provided by theforeman/puppet-puppet, so we configure APT to avoid
  # installing recommended packages.
  #
  # TODO: evaluate if we can avoid this by somehow making sure that this
  #       module is executed after Nginx is already installed.
  ensure_resource('apt::conf', 'norecommends', {
    priority => '00',
    content  => "Apt::Install-Recommends 0;\nApt::AutoRemove::InstallRecommends 1;\n",
  })

  ensure_packages([ 'gitweb', 'fcgiwrap' ], { require => Apt::Conf['norecommends'], })

  file { '/etc/gitweb.conf':
    ensure  => present,
    content => epp('profile/gitweb/gitweb.conf.epp', { 'projectroot'    => $projectroot }),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  #
  # Web frontend with Nginx
  #

  include profile::apt
  include profile::nginx

  nginx::resource::server { $domain:
    www_root    => '/usr/share/gitweb',
    index_files => [ 'index.cgi' ],
  }

  nginx::resource::location { '/index.cgi':
    server              => $domain,
    www_root            => '/usr/share/gitweb',
    fastcgi             => 'unix:/run/fcgiwrap.socket',
    fastcgi_param       => { 'GITWEB_CONFIG' => '/etc/gitweb.conf' },
    location_cfg_append => { 'gzip' => 'off' },
  }

  #
  # Serve repositories over HTTP
  #
  nginx::resource::location { '~ ^.*\.git/(HEAD|info/refs|objects/info/.*|git-(upload|receive)-pack)$':
    server         => $domain,
    www_root       => $projectroot,
    fastcgi        => 'unix:/run/fcgiwrap.socket',
    fastcgi_params => 'fastcgi_params',
    fastcgi_param  => {
      'SCRIPT_FILENAME'     => '/usr/lib/git-core/git-http-backend',
      'REMOTE_USER'         => '$remote_user',
      'PATH_INFO'           => '$uri',
      'GIT_PROJECT_ROOT'    => '/var/lib/gitolite3/repositories',
      'GIT_HTTP_EXPORT_ALL' => '""',  # Only serve repos containing a `git-daemon-export-ok` file
    },
  }


  firewall { '100 allow HTTP access':
    source => '192.168.122.0/24',
    dport  => 80,
    proto  => 'tcp',
    action => 'accept',
  }

  @@profile::reverse_proxy { $domain:
    proxy => "http://${::fqdn}",
    ssl   => $ssl,
  }

}
