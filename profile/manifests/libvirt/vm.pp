# Manage a VM
#
# Note: Libvirt will not be automatically restarted on VM resources update.
#
define profile::libvirt::vm (
  Integer $cpus          = 1,
  Integer $memory        = 512,
  String  $disk          = '5G',
  String  $volume_group  = 'default',
  String  $template      = 'debian',
  String  $network       = 'default',
  String  $puppet_server = 'puppet',
) {

  lvm::logical_volume { $name:
    volume_group => $volume_group,
    size         => $disk,
    mounted      => false,
    createfs     => false,
  }

  $clone_cmd = '/usr/local/sbin/clone-vm'
  $device = "/dev/${volume_group}/${name}"
  $xml_file = "/etc/libvirt/qemu/${name}.xml"

  exec { "clone ${name}":
    command => "${clone_cmd} ${name} ${cpus} ${memory} network=${network} ${device} /var/lib/libvirt/images/${template}.raw ${puppet_server}",  # lint:ignore:140chars
    creates => $xml_file,
    timeout => 300,
    require => [
      Lvm::Logical_volume[$name],
      File[$clone_cmd],
    ],
  }

  file { $xml_file:
    owner   => root,
    group   => root,
    mode    => '0600',
    require => Exec["clone ${name}"],
  }

  # The domain definition below currently doesn't do much because the domain
  # XML file is created by our own custom script that runs `virt-install`.

  libvirt::domain { $name:
    devices_profile => 'default',
    dom_profile     => 'default',
    disks           => [{
      'type'   => 'volume',
      'device' => 'disk',
      'source' => { 'dev' => $device},
    }],
    domconf         => {
      'memory'        => {
        'attrs'  => { 'unit' => 'MiB' },
        'values' => $memory,
      },
      'currentMemory' => {
        'attrs'  => { 'unit' => 'MiB' },
        'values' => $memory,
      },
    },
    interfaces      => [{ 'network' => $network }],
    autostart       => true,
    require         => Exec["clone ${name}"],
  }

  # The domain definition above also doesn't actually update the domain XML
  # file when resources are changed, so we need to explicitely do this.

  $memory_mib = $memory * 1024

  augeas { "set resources of VM ${name}":
    incl    => $xml_file,
    lens    => 'Xml.lns',
    changes => [
      "set domain/memory/#text ${memory_mib}",
      "set domain/currentMemory/#text ${memory_mib}",
      "set domain/vcpu/#text ${cpus}",
      "set domain//devices/disk/source/#attribute/dev ${device}",
    ],
    require => File[$xml_file],
  }

  # TODO: reload libvirtd (notifying Service['libvirtd'] currently causes a
  #       dependency loop).

}
