# Libvirt Template VM
#
# This class generates `.qcow2` and `.raw` images under `/var/lib/libvirt/images`
# that can be used to clone VMs.
#
# Note: This will generate an SSH key for root so it can be embedded in the
#       image.
#
# Note: Uses network and bridge both called "template" for installation.
#
class profile::libvirt::template (
  String $vm_name = 'debian',
) {

  $network = 'template'

  profile::libvirt::network { $network:
    bridge       => $network,
    forward_mode => 'open',
    ip_address   => '192.168.121.1',
    ip_netmask   => '255.255.255.0',
    dhcp_start   => '192.168.121.10',
    dhcp_end     => '192.168.121.250',
    subnet       => '192.168.121.0/24',
    autostart    => false,
  }

  ssh_keygen { 'root':
    type     => 'ed25519',
    filename => '/root/.ssh/id_ed25519',
  }

  $preseed_file = '/var/lib/libvirt/preseed.cfg'

  file { $preseed_file:
    content => epp('profile/libvirt/preseed.cfg.epp', { 'hostname' => $vm_name }),
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    require => Ssh_keygen['root'],
  }

  $install_vm_cmd = '/usr/local/sbin/install-vm'

  file { $install_vm_cmd:
    source => 'puppet:///modules/profile/libvirt/install-vm',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  $qemu_image = "/var/lib/libvirt/images/${vm_name}.qcow2"

  exec { "install ${vm_name}":
    command => "${install_vm_cmd} ${vm_name} 2 1024 ${network} size=5",
    creates => $qemu_image,
    timeout => 1500,
    require => [
      Profile::Libvirt::Network[$network],
      File[$install_vm_cmd],
      File[$preseed_file],
    ]
  }

  $raw_image = "/var/lib/libvirt/images/${vm_name}.raw"

  exec { "create raw image for ${vm_name}":
    command => "/usr/bin/qemu-img convert -O raw ${qemu_image} ${raw_image}",
    creates => $raw_image,
    require => Exec["install ${vm_name}"],
  }

}
