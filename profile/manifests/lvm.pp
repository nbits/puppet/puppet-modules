# Configure LVM
class profile::lvm (
  String $physical_volume = '/dev/mapper/default',
  String $volume_group    = 'default',
) {

  class { 'lvm':
    manage_pkg => true,
  }

  physical_volume { $physical_volume:
    ensure => present,
  }

  exec { "make PV ${physical_volume} allocatable":
    command => "/usr/sbin/pvchange --allocatable y ${physical_volume}",
    unless  => "/usr/sbin/pvdisplay ${physical_volume} | grep 'Allocatable\\s\\+yes'",
    require => Physical_volume[$physical_volume],
  }

  volume_group { $volume_group:
    ensure           => present,
    physical_volumes => $physical_volume,
    require          => Exec["make PV ${physical_volume} allocatable"],
  }

}
