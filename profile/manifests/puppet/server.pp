# Run a Puppet Server supported by Gitolite
class profile::puppet::server (
  String $server = 'puppet',
  Stdlib::IP::Address::V4::Nosubnet $server_ip = '127.0.0.1',
  Integer $server_port = 8140,
  Hash $trusted_keys = {},
  String $reports_max_age = '1w',
) {

  include profile::apt

  $git_repo_path = '/var/lib/gitolite3/repositories/puppet.git'

  class { 'profile::puppet::server::git':
    git_repo_path => $git_repo_path,
    trusted_keys  => $trusted_keys,
  }

  # The theforeman/puppet module needs these to exist, but Debian doesn't
  # provide them
  file { [
    '/opt',
    '/opt/puppetlabs',
    '/opt/puppetlabs/server',
    '/opt/puppetlabs/server/apps',
    '/opt/puppetlabs/server/apps/puppetserver',
    '/etc/puppet/conf.d',
  ]:
    ensure => directory,
  }

  host { 'puppet':
    ip => '127.0.0.1',
  }

  $conf_dir = '/etc/puppet'

  $real_server_ip = $server_ip ? {
    '0.0.0.0' => '127.0.0.1',
    default   => $server_ip,
  }

  class { 'profile::puppet::server::onion_service':
    server_ip   => $real_server_ip,
    server_port => $server_port,
  }

  class { 'puppet':
    agent                      => true,
    dns_alt_names              => [ 'puppet', $facts['puppet_onion_address'] ],
    runmode                    => 'systemd.timer',
    server                     => true,
    server_reports             => 'store,puppetdb',
    server_foreman             => false,
    server_external_nodes      => '/usr/local/bin/puppet_node_classifier',
    server_environments_owner  => 'gitolite3',
    server_environments_group  => 'gitolite3',
    server_git_repo            => true,
    server_git_repo_path       => $git_repo_path,
    server_git_repo_user       => 'gitolite3',
    server_git_repo_group      => 'gitolite3',
    server_ip                  => $server_ip,
    server_port                => $server_port,
    server_storeconfigs        => true,
    agent_server_hostname      => $server,
    require                    => [
      Package['gitolite3'],
      Tor::Daemon::Onion_service['puppet'],
    ],
  }

  class { 'profile::puppet::server::puppetdb':
    puppet_ssl_key_path     => "${puppet::ssldir}/private_keys/${trusted['certname']}.pem",
    puppet_ssl_cert_path    => "${puppet::ssldir}/certs/${trusted['certname']}.pem",
    puppet_ssl_ca_cert_path => "${puppet::ssldir}/certs/ca.pem",
    require                 => [
      Host['puppet'],
    ]
  }

  include profile::puppet::server::enc

  class { 'profile::puppet::server::firewall':
    server_port => $server_port,
  }

  class { 'profile::puppet::server::eyaml':
    conf_dir => $conf_dir,
    require  => Class['puppet'],
  }

  tidy { '/var/lib/puppetserver/reports':
    age     => $reports_max_age,
    recurse => true,
    require => Class['puppet'],
  }

}
