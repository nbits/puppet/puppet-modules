# A Puppet Agent's firewall
class profile::puppet::agent::firewall (
  Stdlib::IP::Address::V4::Nosubnet $ip = lookup('profile::tinc::ip', String, 'first', $facts['networking']['ip']),
) {

  $tag_environment = "environment:${::environment}"

  # Export a rule so the server allows us to connect to it
  @@firewall { "100 accept TCP on Puppet Server port from ${::fqdn}":
    proto  => 'tcp',
    source => $ip,
    action => 'accept',
    tag    => [ 'puppet_agent', $tag_environment ],
  }

}
