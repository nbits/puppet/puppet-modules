# Firewall rules for a Puppet Server
class profile::puppet::server::firewall (
  Stdlib::IP::Address::V4::Nosubnet $server_ip = '127.0.0.1',
  Integer $server_port = 8140,
) {

  $tag_environment = "environment:${::environment}"

  Firewall {
    destination => $server_ip,
    dport       => $server_port,
  }

  # Allow Puppet Agents to connect to our Puppet Server port
  Firewall <<| tag == 'puppet_agent' and tag == $tag_environment |>>

}
