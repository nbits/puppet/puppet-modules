# Eyaml support
class profile::puppet::server::eyaml (
  Stdlib::Absolutepath $conf_dir = '/etc/puppet',
) {

  ensure_packages(['hiera-eyaml'])

  file { "${conf_dir}/keys":
    ensure => directory,
    mode   => '0500',
    owner  => puppet,
  }

  $private_key = "${conf_dir}/keys/private_key.pkcs7.pem"
  $public_key = "${conf_dir}/keys/public_key.pkcs7.pem"

  exec { '/usr/bin/eyaml createkeys':
    user    => 'puppet',
    cwd     => $conf_dir,
    creates => $private_key,
    require => [ File["${conf_dir}/keys"] , Package['hiera-eyaml'] ],
  }

  file { $private_key:
    ensure  => file,
    mode    => '0400',
    owner   => puppet,
    group   => puppet,
    require => Exec['/usr/bin/eyaml createkeys'],
  }

  file { $public_key:
    ensure  => file,
    mode    => '0400',
    owner   => puppet,
    group   => puppet,
    require => Exec['/usr/bin/eyaml createkeys'],
  }

}
