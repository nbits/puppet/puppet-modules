# Configure a Git environment with Gitolite and Gitweb
class profile::puppet::server::git (
  Stdlib::Absolutepath $git_repo_path = '/var/lib/gitolite3/repositories/puppet.git',
  Hash $trusted_keys = {},
  String $cert_nonce_seed = String(fqdn_rand(9223372036854775807)),
) {

  include profile::gitolite3

  file { '/var/lib/gitolite3/repositories':
    ensure  => directory,
    owner   => 'gitolite3',
    group   => 'www-data',
    mode    => '0750',
    require => [
      Package['gitolite3'],
      File['/var/lib/gitolite3'],
    ],
  }

  ## Require signed pushes to production

  $settings = {
    'receive' => {
      'advertisePushOptions' =>  'true',  # lint:ignore:quoted_booleans
      'certNonceSeed' => $cert_nonce_seed,
    }
  }

  inifile::create_ini_settings($settings, { path => "${git_repo_path}/config" })

  $protected_branch = 'production'

  file { "${git_repo_path}/hooks/pre-receive":
    ensure  => present,
    content => epp('profile/gitolite/pre-receive.epp', {
      protected_branch => $protected_branch,
    }),
    owner   => 'gitolite3',
    group   => 'gitolite3',
    mode    => '0700',
  }

  # We use a file to detect and set the state of the owner trust database.
  $trustdb_path = "${git_repo_path}/hooks/trustdb"

  file { $trustdb_path:
    ensure  => present,
    content => epp('profile/gitolite/trustdb.epp', {
      trusted_keys => $trusted_keys,
    }),
    owner   => 'gitolite3',
    group   => 'gitolite3',
    mode    => '0600',
  }

  file { '/var/lib/gitolite3/openpgp':
    ensure  => directory,
    owner   => 'gitolite3',
    group   => 'gitolite3',
    mode    => '0700',
    require => [
      Package['gitolite3'],
      File['/var/lib/gitolite3'],
    ],
  }

  $trusted_keys.each | String $fingerprint, String $content | {
    file { "/var/lib/gitolite3/openpgp/${fingerprint}.asc":
      ensure  => file,
      content => $content,
      owner   => 'gitolite3',
      group   => 'gitolite3',
      mode    => '0600',
      require => File['/var/lib/gitolite3/openpgp'],
    }
    exec { "Ensure Gitolite\'s keyring contains trusted key: ${fingerprint}":
      command => "/usr/bin/gpg --import < /var/lib/gitolite3/openpgp/${fingerprint}.asc",
      unless  => "/usr/bin/gpg -k --with-colons | grep ${fingerprint}",
      user    => 'gitolite3',
      require => [
        File['/var/lib/gitolite3'],
        File["/var/lib/gitolite3/openpgp/${fingerprint}.asc"],
      ],
    } -> Exec['Ensure Gitolite has \'ultimate trust\' on trusted keys']
  }

  exec { 'Ensure Gitolite has \'ultimate trust\' on trusted keys':
    command => "/usr/bin/rm -f /var/lib/gitolite3/.gnupg/trustdb.gpg; /usr/bin/gpg --import-ownertrust < ${trustdb_path}",
    user    => 'gitolite3',
    unless  => "/usr/bin/gpg --export-ownertrust | grep -v \\# | sort | diff ${trustdb_path} -",
    require => [
      File['/var/lib/gitolite3'],
      File[$trustdb_path],
    ],
  }

}
