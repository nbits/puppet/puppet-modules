# Run a Puppet Agent periodically
class profile::puppet::agent (
  String $server = 'puppet',
  Optional[String] $server_ip = undef,
  Stdlib::IP::Address::V4::Nosubnet $ip = lookup('profile::tinc::ip', String, 'first', $facts['networking']['ip']),
) {

  class { 'profile::puppet::agent::firewall':
    ip => $ip,
  }

  file { [
    '/opt',
    '/opt/puppetlabs',
    '/etc/puppet/conf.d',
  ]:
    ensure => directory,
  }

  if $server_ip {
    host { $server:
      ip => $server_ip,
    }
    $puppet_require = Host[$server]
  } else {
    $puppet_require = undef
  }

  class { 'puppet':
    manage_packages       => false,
    agent                 => true,
    runmode               => 'systemd.timer',
    ssldir                => '/var/lib/puppet/ssl',
    vardir                => '/var/lib/puppet',
    sharedir              => '/usr/share/puppet',
    agent_server_hostname => $server,
    require               => $puppet_require,
  }

  puppet::config::main { 'certificate revocation policy':
    key   => 'certificate_revocation',
    value => 'leaf',
  }

}
