# Monitoring of Nextcloud server
class profile::nextcloud::monitoring (
  String $domain = 'example.org',
  Optional[String] $token = undef,
) {

  $vars = $token ? {
    String => {
      http_header => "Nc-Token: ${token}",
      http_string => '<status>ok</status>',
      http_uri    => '/ocs/v2.php/apps/serverinfo/api/v1/info',
    },
    default => {},
  }

  @@profile::monitoring::services::http { $domain:
    config => {
      host_name => $trusted['certname'],
      vars      => $vars,
    },
    tag    => "environment:${::environment}",
  }

}
