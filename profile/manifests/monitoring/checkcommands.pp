# A CheckCommand for host memory
class profile::monitoring::checkcommands {

  # XXX: Workaround so this class can be compiled by itself. Without this,
  # declaration of Icinga2 objects fail because of missing variables.
  if ! defined(Class['icinga2']) {
    include icinga2
  }

  ### Memory

  ensure_packages([ 'dc' ])

  file { '/usr/local/bin/check_mem_ng.sh':
    ensure  => file,
    source  => 'puppet:///modules/profile/monitoring/check_mem_ng.sh',
    owner   => root,
    group   => root,
    mode    => '0755',
    require => Package['dc'],
  }

  icinga2::object::checkcommand{ 'memory':
    command   => [ '/usr/local/bin/check_mem_ng.sh' ],
    arguments => {
      '-w' => { value => '$mem_wused$' },
      '-c' => { value => '$mem_cused$' },
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
    require   => File['/usr/local/bin/check_mem_ng.sh'],
  }

  ### Systemd

  file { '/usr/lib/nagios/plugins/check_systemd.py':
    ensure => file,
    source => 'puppet:///modules/profile/monitoring/check_systemd',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  if $facts['os']['distro']['codename'] == 'bullseye' {
    icinga2::object::checkcommand{ 'systemd':
      command   => [ '/usr/lib/nagios/plugins/check_systemd.py' ],
      arguments => {
        '-u' => { value => '$systemd_unit$' },
        '-e' => { value => '$systemd_exclude_unit$' },
        '-w' => { value => '$systemd_warning$' },
        '-c' => { value => '$systemd_critical$' },
      },
      target    => '/etc/icinga2/conf.d/checkcommands.conf',
      require   => File['/usr/lib/nagios/plugins/check_systemd.py']
    }
  }

  ### Duplicity backups

  file { '/usr/local/bin/check_backup':
    ensure => file,
    source => 'puppet:///modules/profile/monitoring/check_backup',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  icinga2::object::checkcommand{ 'backup':
    command   => [ '/usr/local/bin/check_backup' ],
    arguments => {
      '-n' => {
        value      => '$node$',
        order      => 1,
        required   => true,
        repeat_key => false,
        skip_key   => true,
      },
      '-b' => {
        value      => '$basedir$',
        order      => 2,
        required   => true,
        repeat_key => false,
        skip_key   => true,
      },
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
    require   => File['/usr/local/bin/check_backup'],
  }

  ### Borg backups

  file { '/usr/local/bin/check_borg_backup':
    ensure => file,
    source => 'puppet:///modules/profile/monitoring/check_borg_backup',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/usr/local/bin/check_borg_backup_setuid':
    ensure  => file,
    source  => 'puppet:///modules/profile/monitoring/check_borg_backup_setuid',
    owner   => 'root',
    group   => 'nagios',
    mode    => '4750',
    require => Package['icinga2'],
  }

  icinga2::object::checkcommand{ 'borg':
    command   => [ '/usr/local/bin/check_borg_backup_setuid' ],
    arguments => {
      'nokey1' => {
        value      => '$borg_user$',
        order      => 1,
        required   => true,
        repeat_key => false,
        skip_key   => true,
      },
      'nokey2' => {
        value      => '$borg_host$',
        order      => 2,
        required   => true,
        repeat_key => false,
        skip_key   => true,
      },
      'nokey3' => {
        value      => '$borg_path$',
        order      => 2,
        required   => true,
        repeat_key => false,
        skip_key   => true,
      },
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
    require   => File['/usr/local/bin/check_borg_backup'],
  }

  ### Borg backups - v2

  file { '/usr/local/bin/check_borg_log':
    ensure => file,
    source => 'puppet:///modules/profile/monitoring/check_borg_log',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  icinga2::object::checkcommand{ 'borg_log':
    command   => [ '/usr/local/bin/check_borg_log' ],
    arguments => {
      'nokey1' => {
        value      => '$borg_log_file$',
        order      => 1,
        required   => true,
        repeat_key => false,
        skip_key   => true,
      },
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
    require   => File['/usr/local/bin/check_borg_log'],
  }

  ### Mumble

  file { '/usr/local/bin/check_mumble':
    ensure => file,
    source => 'puppet:///modules/profile/monitoring/check_mumble',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  icinga2::object::checkcommand{ 'mumble':
    command   => [ '/usr/local/bin/check_mumble' ],
    arguments => {
      '-a' => {
        value      => '$address$',
        order      => 1,
        required   => true,
        repeat_key => false,
        skip_key   => true,
      },
      '-p' => {
        value      => '$port$',
        order      => 2,
        required   => true,
        repeat_key => false,
        skip_key   => true,
      },
    },
    vars      => { port => '64738', },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
    require   => File['/usr/local/bin/check_mumble'],
  }

  ### needrestart

  ensure_packages([ 'needrestart' ])

  sudo::conf { 'nagios-needrestart':
    priority => 20,
    content  => 'nagios ALL=NOPASSWD: /usr/sbin/needrestart -p, /usr/sbin/needrestart -p -k, /usr/sbin/needrestart -p -k -l, /usr/sbin/needrestart -p -k -l -w, /usr/sbin/needrestart -p -l, /usr/sbin/needrestart -p -l -w, /usr/sbin/needrestart -p -w, /usr/sbin/needrestart -p -k -w', # lint:ignore:140chars
  }

  icinga2::object::checkcommand{ 'needrestart':
    command   => [ '/usr/bin/sudo', '/usr/sbin/needrestart', '-p' ],
    arguments => {
      '-k' => {
        description => 'check for obsolete kernel',
        set_if      => '$needrestart_kernel$',
      },
      '-l' => {
        description => 'check for obsolete libraries',
        set_if      => '$needrestart_libraries$',
      },
      '-w' => {
        description => 'check for obsolete CPU microcode',
        set_if      => '$needrestart_microcode$',
      },
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
    require   => Sudo::Conf['nagios-needrestart'],
  }

}
