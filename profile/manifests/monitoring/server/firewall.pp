# Icinga2 server firewall
class profile::monitoring::server::firewall (
  Stdlib::IP::Address::V4::Nosubnet $ip = lookup('profile::tinc::ip', String, 'first', $facts['networking']['ip']),
) {

  $tag_environment = "environment:${::environment}"

  # Export a rule so all clients allow the server to connect to them
  @@firewall { "100 accept TCP on Icinga2 API port from ${::fqdn}":
    proto  => 'tcp',
    source => $ip,
    dport  => 5665,
    action => 'accept',
    tag    => [ 'icinga_server_api', $tag_environment ],
  }

  Firewall {
    destination => $ip,
  }

  # Allow Icinga2 agents to connect to our Icinga2 API
  Firewall <<| tag == 'icinga_agent_api' and tag == $tag_environment |>>

  # Allow Graphite relays to connect to our API
  Firewall <<| tag == 'carbon_relay' and tag == $tag_environment |>>

}
