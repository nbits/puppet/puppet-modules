# Expose the Icinga2 API as an onion service
class profile::monitoring::server::onion_service (
  String $server_ip = '127.0.0.1',
  Integer $server_port = 5665,
) {

  include tor

  tor::daemon::onion_service { 'icinga2':
    ports => [ "${server_port} ${server_ip}" ],
  }

}
