# Icinga2 web interface (icingaweb2)
class profile::monitoring::server::web (
  String $domain_name = $trusted['certname'],
  String $dbpass = 'icingaweb2',
  String $webdbpass = 'changeme!',
  String $apipasswd = 'changeme!',
  String $target_email = 'admin@example.org',
  Boolean $tls = false,
) {

  include profile::nginx
  include profile::letsencrypt
  include profile::firewall::web

  profile::website { $domain_name:
    ssl                  => $tls,
    ssl_cert             => "/var/local/tls/${domain_name}/fullchain.pem",
    ssl_key              => "/var/local/tls/${domain_name}/privkey.pem",
    index_files          => [],
    use_default_location => false,
  }

  nginx::resource::location { 'root':
    location            => '/',
    server              => $domain_name,
    index_files         => [],
    location_cfg_append => {
      rewrite => '^/(.*) https://$host/icingaweb2/$1 permanent'
    },
    ssl_only            => $tls,
  }

  nginx::resource::location { 'icingaweb2_index':
    location      => '~ ^/icingaweb2/index\.php(.*)$',
    server        => $domain_name,
    index_files   => [],
    fastcgi       => '127.0.0.1:9000',
    fastcgi_index => 'index.php',
    fastcgi_param => {
      'ICINGAWEB_CONFIGDIR' => '/etc/icingaweb2',
      'REMOTE_USER'         => '$remote_user',
      'SCRIPT_FILENAME'     => '/usr/share/icingaweb2/public/index.php',
    },
    ssl_only      => $tls,
  }

  nginx::resource::location { 'icingaweb':
    location       => '~ ^/icingaweb2(.+)?',
    location_alias => '/usr/share/icingaweb2/public',
    try_files      => ['$1', '$uri', '$uri/', '/icingaweb2/index.php$is_args$args'],
    index_files    => ['index.php'],
    server         => $domain_name,
    ssl_only       => $tls,
  }

  ### PHP

  class { 'php':
    fpm        => true,
    fpm_user   => 'www-data',
    fpm_group  => 'www-data',
    extensions => {
      'mysql' => {
        so_name    => 'mysqlnd',
        ini_prefix => '30-',
      }
    }
  }

  # install required packages
  include ::mysql::server

  mysql::db { 'icingaweb2':
    user     => 'icingaweb2',
    password => $webdbpass,
    host     => 'localhost',
    grant    => ['SELECT', 'INSERT', 'UPDATE', 'DELETE', 'DROP', 'CREATE VIEW', 'CREATE', 'INDEX', 'EXECUTE', 'ALTER', 'REFERENCES'],
  }

  class {'icingaweb2':
    manage_repos  => false,
    import_schema => true,
    db_type       => 'mysql',
    db_host       => 'localhost',
    db_port       => 3306,
    db_username   => 'icingaweb2',
    db_password   => $webdbpass,
    conf_user     => 'www-data',
    require       => Mysql::Db['icingaweb2'],
  }

  class { 'icingaweb2::module::monitoring':
    ido_host          => 'localhost',
    ido_db_name       => 'icinga2',
    ido_db_username   => 'icinga2',
    ido_db_password   => $dbpass,
    commandtransports => {
      icinga2 => {
        transport => 'api',
        username  => 'icingaweb2',
        password  => $apipasswd,
      }
    }
  }

  # XXX: Workaround so this class can be compiled by itself. Without this,
  # declaration of Icinga2 objects fail because of missing variables.
  if ! defined(Class['icinga2']) {
    include icinga2
  }

  icinga2::object::apiuser { 'icingaweb2':
    password    => $apipasswd,
    permissions => [ '*' ],
    target      => '/etc/icinga2/conf.d/apiusers.conf',
  }

}
