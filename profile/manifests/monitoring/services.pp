# Monitored services
class profile::monitoring::services (
  Hash $http = {},
  Hash $smtp = {},
  Hash $dns = {},
  Hash $redirects = {},
  Hash $backup_targets = {},
  Array $backup_sources = [],
  Hash $services = {},
  Hash $service_groups = {},
  Integer $ssh_port = 22,
  String $default_service_check_interval = '1m',
  String $default_service_retry_interval = '30s',
  Boolean $apt_only_critical = false,
  String $http_service_check_interval = '30m',
  String $http_service_retry_interval = '5m',
  String $backup_service_check_interval = '2h',
  String $backup_service_retry_interval = '10m',
  Optional[String] $http_endpoint = undef,
) {

  # XXX: Workaround so this class can be compiled by itself. Without this,
  # declaration of Icinga2 objects fail because of missing variables.
  if ! defined(Class['icinga2']) {
    include icinga2
  }

## services running on the monitoring server

  icinga2::object::service { 'generic-service':
    template           => true,
    target             => '/etc/icinga2/conf.d/templates.conf',
    check_interval     => $default_service_check_interval,
    retry_interval     => $default_service_retry_interval,
    max_check_attempts => 3,
    check_command      => 'hostalive',
  }

# make sure each host is alive

  icinga2::object::service { 'Hostalive (VPN)':
    check_command => 'hostalive',
    apply         => true,
    assign        => [ true ],
    import        => [ 'generic-service' ],
    target        => '/etc/icinga2/conf.d/services.conf',
    groups        => [ 'ping' ],
  }

  # APT check config for the host itself is installed by icinga2-common, let's
  # remove it and add a general APT check for all hosts.
  file { '/etc/icinga2/conf.d/apt.conf':
    ensure => absent,
  }

  icinga2::object::service { 'APT':
    check_command    => 'apt',
    apply            => true,
    command_endpoint => 'host_name',
    zone             => 'icinga2',
    assign           => [ true ],
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    vars             => {
      apt_only_critical => $apt_only_critical,
    },
    require          => File['/etc/icinga2/conf.d/apt.conf'],
    groups           => [ 'apt' ],
  }

  icinga2::object::service { 'Reboot required':
    check_command    => 'file_age',
    apply            => true,
    command_endpoint => 'host_name',
    zone             => 'icinga2',
    assign           => [ true ],
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    vars             => {
      file_age_file          => '/var/run/reboot-required',
      file_age_critical_time => 1,
      file_age_ignoremissing => true,
    },
    groups           => [ 'reboot-required' ],
  }

  icinga2::object::service { 'Memory':
    check_command    => 'memory',
    apply            => true,
    command_endpoint => 'host_name',
    zone             => 'icinga2',
    assign           => [ true ],
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    vars             => {
      mem_wused => '90',
      mem_cused => '95',
    },
    groups           => [ 'memory' ],
  }

  icinga2::object::service { 'CPU Load':
    check_command    => 'load',
    apply            => true,
    command_endpoint => 'host_name',
    zone             => 'icinga2',
    assign           => [ true ],
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    vars             => {
      load_wload1  => 5,
      load_wload5  => 4,
      load_wload15 => 3,
      load_cload1  => 10,
      load_cload5  => 6,
      load_cload15 => 4,
      load_percpu  => true,
    },
    groups           => [ 'cpu' ],
  }

  icinga2::object::service { 'Disk':
    check_command    => 'disk',
    apply            => true,
    command_endpoint => 'host_name',
    zone             => 'icinga2',
    assign           => [ true ],
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    vars             => {
      disk_wfree => '10%',
      disk_cfree => '5%',
    },
    groups           => [ 'disk' ],
  }

  icinga2::object::service { 'SSH (local)':
    check_command    => 'ssh',
    apply            => true,
    command_endpoint => 'host_name',
    zone             => 'icinga2',
    assign           => [ true ],
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    vars             => {
      ssh_port    => $ssh_port,
      ssh_address => '127.0.0.1',
    },
    groups           => [ 'ssh' ],
  }

  icinga2::object::service { 'SSH (remote)':
    check_command => 'ssh',
    apply         => true,
    assign        => [ 'host.vars.ssh_remote_address != false' ],
    import        => [ 'generic-service' ],
    target        => '/etc/icinga2/conf.d/services.conf',
    vars          => {
      ssh_port    => $ssh_port,
      ssh_address => 'host.vars.ssh_remote_address',
    },
    groups        => [ 'ssh' ],
  }

  icinga2::object::service { 'Mail Queue':
    check_command    => 'mailq',
    apply            => true,
    command_endpoint => 'host_name',
    zone             => 'icinga2',
    assign           => [ true ],
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    vars             => {
      mailq_warning  => 3,
      mailq_critical => 10,
    },
    groups           => [ 'email' ],
  }

  icinga2::object::service { 'Systemd':
    check_command    => 'systemd',
    apply            => true,
    assign           => [ true ],
    command_endpoint => 'host_name',
    zone             => 'icinga2',
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    groups           => [ 'systemd' ],
  }

  icinga2::object::service { 'Need Restart':
    check_command    => 'needrestart',
    apply            => true,
    assign           => [ true ],
    command_endpoint => 'host_name',
    zone             => 'icinga2',
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    groups           => [ 'needrestart' ],
  }

# check our http services

  $ip_vars = {
    'http_ipv4' => true,
    'http_ipv6' => false,
  }

  $https_vars = {
    'http_port'                        => 443,
    'http_ssl'                         => true,
    'http_ssl_force_tlsv1_2_or_higher' => true,
  }

  Profile::Monitoring::Services::Http <<| tag == "environment:${::environment}" |>>

  $http.each | String $site, Hash $config | {
    $name_vars = {
      'http_vhost'   => $site,
      'http_address' => $site,
    }
    $conn_vars = deep_merge($ip_vars, $https_vars)
    $default_vars = deep_merge($conn_vars,$name_vars)
    $vars = deep_merge($default_vars,$config['vars'])

    $custom_groups = $config['groups'] ? {
      undef   => [],
      default => $config['groups'],
    }

    $http_redirect_vars = deep_merge($name_vars, {
      http_expect       => 'HTTP/1.1 301 Moved Permanently',
      http_headerstring => "Location: https://${site}/",
    })

    icinga2::object::service{ "HTTP Redirect: ${site}":
      check_command      => 'http',
      max_check_attempts => 23,
      host_name          => $config['host_name'],
      zone               => 'icinga2',
      command_endpoint   => $http_endpoint,
      vars               => $http_redirect_vars,
      groups             => [ 'websites' ] + $custom_groups,
      import             => [ 'generic-service' ],
      target             => '/etc/icinga2/conf.d/services.conf',
      check_interval     => $http_service_check_interval,
      retry_interval     => $http_service_retry_interval,

    }

    icinga2::object::service{ "HTTPS: ${site}":
      check_command      => 'http',
      max_check_attempts => 23,
      host_name          => $config['host_name'],
      zone               => 'icinga2',
      command_endpoint   => $http_endpoint,
      vars               => $vars,
      groups             => [ 'websites' ] + $custom_groups,
      import             => [ 'generic-service' ],
      target             => '/etc/icinga2/conf.d/services.conf',
      check_interval     => $http_service_check_interval,
      retry_interval     => $http_service_retry_interval,
    }

    icinga2::object::service{ "TLS: ${site}":
      check_command    => 'ssl_cert',
      host_name        => $config['host_name'],
      zone             => 'icinga2',
      command_endpoint => $http_endpoint,
      vars             => {
        ssl_cert_address  => $site,
        ssl_cert_cn       => $site,
        ssl_cert_port     => $vars['http_port'],
        ssl_cert_warn     => 14,
        ssl_cert_critical => 7,
      },
      groups           => [ 'certs' ] + $custom_groups,
      import           => [ 'generic-service' ],
      target           => '/etc/icinga2/conf.d/services.conf',
      check_interval   => $http_service_check_interval,
      retry_interval   => $http_service_retry_interval,
    }

  }

  $redirects.each | String $site, Hash $config | {

    $vars = deep_merge($https_vars, {
      http_vhost        => $site,
      http_address      => $site,
      http_expect       => 'HTTP/1.1 301 Moved Permanently',
      http_headerstring => "Location: ${config['location']}",
    })

    icinga2::object::service{ "HTTP Redirect: ${site}":
      check_command      => 'http',
      max_check_attempts => 23,
      host_name          => $config['host_name'],
      zone               => 'icinga2',
      command_endpoint   => $http_endpoint,
      vars               => $vars,
      import             => [ 'generic-service' ],
      target             => '/etc/icinga2/conf.d/services.conf',
      check_interval     => $http_service_check_interval,
      retry_interval     => $http_service_retry_interval,
    }

    icinga2::object::service{ "TLS: ${site}":
      check_command    => 'ssl_cert',
      host_name        => $config['host_name'],
      zone             => 'icinga2',
      command_endpoint => $http_endpoint,
      vars             => {
        ssl_cert_address  => $site,
        ssl_cert_cn       => $site,
        ssl_cert_port     => 443,
        ssl_cert_warn     => 14,
        ssl_cert_critical => 7,
      },
      import           => [ 'generic-service' ],
      target           => '/etc/icinga2/conf.d/services.conf',
      check_interval   => $http_service_check_interval,
      retry_interval   => $http_service_retry_interval,
    }

  }

  ### Backup checks

  $backup_targets.each | String $target, $basedir | {

    $backup_sources.each | String $source | {

      icinga2::object::service { "Duplicity backup of ${source} on ${target}":
        check_command    => 'backup',
        host_name        => $target,
        command_endpoint => $target,
        zone             => 'icinga2',
        vars             => {
          'node'    => $source,
          'basedir' => $basedir,
        },
        import           => [ 'generic-service' ],
        target           => '/etc/icinga2/conf.d/services.conf',
        groups           => [ 'backups' ],
        check_interval   => $backup_service_check_interval,
        retry_interval   => $backup_service_retry_interval,
      }

    }

  }

  ### Extra service objects

  $services.each | String $service, Hash $config | {

    $defaults = {
      check_command => $service,
      import        => [ 'generic-service' ],
      target        => '/etc/icinga2/conf.d/services.conf',
      zone          => 'icinga2',
    }

    $params = deep_merge($defaults, $config)
    create_resources(icinga2::object::service, $service => $params)

  }

  ### Service groups

  $default_groups = {
    'apt'             => 'APT',
    'backups'         => 'Backups',
    'certs'           => 'TLS certificates',
    'cpu'             => 'CPU',
    'email'           => 'E-mail',
    'memory'          => 'Memory',
    'needrestart'     => 'Need Restart',
    'reboot-required' => 'Reboot required',
    'ssh'             => 'SSH',
    'systemd'         => 'Systemd',
    'websites'        => 'Websites',
  }

  $all_groups = merge($default_groups, $service_groups)

  $all_groups.each | String $group, String $display_name | {

    icinga2::object::servicegroup { $group:
      display_name => $display_name,
      target       => '/etc/icinga2/conf.d/servicegroups.conf',
    }

  }

  # Check Borg backups

  Profile::Monitoring::Services::Borg <<| tag == "environment:${::environment}" |>>

}
