# Install collectd and configure it to send metrics to Graphite
class profile::monitoring::collectd {

  package { 'collectd':
    ensure          => installed,
    install_options => [ '--no-install-recommends' ],
  }

  service { 'collectd':
    ensure => running,
    enable => true,
  }

  File <<| tag == 'collectd_graphite' and tag == "environment::${::environment}" |>> ~> Service['collectd']

}
