# Monitoring packages
class profile::monitoring::packages {

  # Some packages might only be available from backports
  include profile::apt

  $plugin_packages = [
    'libmonitoring-plugin-perl',
    'monitoring-plugins',
    'monitoring-plugins-basic',
    'monitoring-plugins-common',
    'monitoring-plugins-standard',
    'monitoring-plugins-contrib',
    'php-curl',
    'python3-nagiosplugin',
  ]

  ensure_packages($plugin_packages, { ensure => latest } )

  if $facts['os']['distro']['codename'] == 'buster' {
    apt::source { 'icinga-stable-release':
      ensure   => present,
      location => 'http://packages.icinga.com/debian',
      release  => "icinga-${facts['os']['distro']['codename']}",
      repos    => 'main',
      key      => {
        id     => 'F51A91A5EE001AA5D77D53C4C6E319C334410682',
        source => 'http://packages.icinga.com/icinga.key',
      },
    }
  }

}
