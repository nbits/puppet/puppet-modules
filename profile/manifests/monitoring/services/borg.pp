# Monitor a borg backup by checking local backup file
define profile::monitoring::services::borg (
  String $source,
  Stdlib::Absolutepath $borg_log_file,
) {

  icinga2::object::service { $name:
    check_command    => 'borg_log',
    host_name        => $source,
    command_endpoint => $source,
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    zone             => 'icinga2',
    vars             => { borg_log_file => $borg_log_file },
    groups           => [ 'backups' ],
  }

}
