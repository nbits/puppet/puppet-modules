# Default services
class profile::monitoring::services::extras (
  Hash $services = {},
) {

  $services.each |String $service, Variant[Hash,Boolean] $config| {

    $defaults = {
      check_command    => $service,
      apply            => true,
      assign           => [ true ],
      command_endpoint => 'host.name',
      import           => [ 'generic-service' ],
      target           => '/etc/icinga2/conf.d/services.conf',
    }

    if $config {
      if has_key($config,'srv'){
        $params = deep_merge($defaults,$config['srv'])
      }
      else {
        $params = $defaults
      }
    }
    else {
      $params = $defaults
    }

    create_resources(icinga2::object::service,$service => $params)

    if $config {
      if has_key($config,'cc') {
        icinga2::object::checkcommand{ $service:
          command   => $config['cc']['cmd'],
          arguments => $config['cc']['args'],
          vars      => $config['cc']['vars'],
          target    => '/etc/icinga2/conf.d/checkcommands.conf',
        }
      }
    }

  }

}
