# An Icinga2 agent firewall
class profile::monitoring::agent::firewall (
  Stdlib::IP::Address::V4::Nosubnet $ip = lookup('profile::tinc::ip', String, 'first', $facts['networking']['ip']),
) {

  $tag_environment = "environment:${::environment}"

  # Export rules so the server allows us to connect to it
  @@firewall { "100 accept TCP on Icinga2 API port from ${::fqdn}":
    proto  => 'tcp',
    source => $ip,
    dport  => 5665,
    action => 'accept',
    tag    => [ 'icinga_agent_api', $tag_environment ],
  }

  @@firewall { "100 accept TCP on Graphite's carbon-cache port from ${::fqdn}":
    proto  => 'tcp',
    source => $ip,
    dport  => 2003,
    action => 'accept',
    tag    => [ 'carbon_relay', $tag_environment ],
  }

  Firewall {
    destination => $ip,
  }

  # Allow the Icinga2 server to connect to our Icinga2 API
  Firewall <<| tag == 'icinga_server_api' and tag == $tag_environment |>>

}
