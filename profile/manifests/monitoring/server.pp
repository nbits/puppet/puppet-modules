# A monitoring server
class profile::monitoring::server (
  String $domain_name = $trusted['certname'],
  String $icingaweb2_domain_name = "icingaweb2.${trusted['certname']}",
  Stdlib::IP::Address::V4::Nosubnet $ip = lookup('profile::tinc::ip', String, 'first', $facts['networking']['ip']),
  String $dbpass = 'changeme!',
  String $webdbpass = 'changeme!',
  String $apipasswd = 'changeme!',
  String $target_email = 'admin@example.com',
  String $ticket_salt = 'changeme!',
  Optional[Stdlib::Ip::Address] $ssh_remote_address = $facts['networking']['ip'],
  Boolean $tls = false,
  Icinga2::CustomAttributes $vars = {},
  Boolean $onion_service = false,
  String $default_host_check_interval = '1m',
  String $default_host_retry_interval = '30s',
  String $default_service_check_interval = '1m',
  String $default_service_retry_interval = '30s',
  Boolean $graphite = true,
) {

  include profile::monitoring::server::firewall

# set up the database

  include ::mysql::server

  mysql::db { 'icinga2':
    user     => 'icinga2',
    password => $dbpass,
    host     => 'localhost',
    grant    => ['SELECT','INSERT','UPDATE','DELETE','DROP','CREATE VIEW','CREATE','INDEX','EXECUTE','ALTER'],
  }

# deploy icinga2

  class { 'icinga2':
    manage_repos => false,
    confd        => true,
    constants    => {
      'NodeName'   => $domain_name,
      'ZoneName'   => 'icinga2',
      'TicketSalt' => $ticket_salt,
    },
  }

  class { 'icinga2::feature::idomysql':
    user          => 'icinga2',
    password      => $dbpass,
    database      => 'icinga2',
    import_schema => true,
    require       => Mysql::Db['icinga2'],
  }

  include icinga2::pki::ca

  class { 'icinga2::feature::api':
    pki             => 'none',
    ca_host         => $domain_name,
    ticket_salt     => $ticket_salt,
    accept_commands => true,
    accept_config   => true,
    endpoints       => { $domain_name => {}, },
    zones           => {
      'icinga2' => {
        'endpoints' => [ $domain_name ],
      },
    },
    ssl_protocolmin => 'TLSv1.2',
  }

  include icinga2::feature::livestatus

  $defaults = {
    'os'               => 'Linux',
    ssh_remote_address => $ssh_remote_address,
  }

  $_vars = deep_merge($defaults, $vars)

  icinga2::object::host { 'generic-host':
    template           => true,
    target             => '/etc/icinga2/conf.d/templates.conf',
    check_interval     => $default_host_check_interval,
    retry_interval     => $default_host_retry_interval,
    max_check_attempts => 3,
    check_command      => 'hostalive',
  }

  icinga2::object::host { $domain_name:
    address => $ip,
    vars    => $_vars,
    import  => [ 'generic-host' ],
    target  => '/etc/icinga2/conf.d/hosts.conf',
    zone    => 'icinga2',
  }

  icinga2::object::zone { 'global-templates':
    global => true,
  }

# set up all our hosts and services

  include profile::monitoring::server::agents
  include profile::monitoring::checkcommands

  class { 'profile::monitoring::services':
    default_service_check_interval => $default_service_check_interval,
    default_service_retry_interval => $default_service_retry_interval,
  }

  include profile::monitoring::services::extras

# add admin user

  icinga2::object::user { 'icingaadmin':
    email  => $target_email,
    target => '/etc/icinga2/conf.d/users.conf',
    #enable_notifications => true,
  }

# add a web interface

  class { 'profile::monitoring::server::web':
    domain_name  => $icingaweb2_domain_name,
    dbpass       => $dbpass,
    webdbpass    => $webdbpass,
    apipasswd    => $apipasswd,
    target_email => $target_email,
    tls          => $tls,
  }

# set up notifications

#  icinga2::object::notification { 'testnotification':
#    target       => '/etc/icinga2/conf.d/test.conf',
#    apply        => true,
#    apply_target => 'Service',
#    assign       => [ 'host.vars.os == Linux' ],
#    users        => ['icingaadmin'],
#    command      => 'mail-service-notification',
#    states       => [ 'Critical', 'Unknown' ],
#    interval     => '1h',
#  }

  @@profile::monitoring::agent::api { "icinga-api-${trusted['certname']}":
    server_domain => $domain_name,
    server_ip     => $ip,
    ticket_salt   => $ticket_salt,
    tag           => [
      "environment:${::environment}",
    ],
  }

  Host <<| tag == 'profile::monitoring::server::domain_name' and tag == "environment:${::environment}" |>>

  ### Tor onion service

  if $onion_service {
    include profile::monitoring::server::onion_service
  }

  ### Graphite integration
  if $graphite {
    include profile::monitoring::server::graphite
    Class['icinga2'] -> Class['profile::monitoring::server::graphite']
  }

}
