# Make sure Puppet is installed either as server or agent
class profile::puppet (
  Enum['agent', 'server'] $type = 'agent',
  String $server = 'puppet',
  Optional[String] $server_ip = undef,
) {

  class { "profile::puppet::${type}":
    server    => $server,
    server_ip => $server_ip,
  }

}
