# Mosh
class profile::mosh {

  ensure_packages(['mosh'])

  firewall { '101 allow Mosh':
    dport  => '60000-60005',
    proto  => 'udp',
    action => 'accept',
  }

}
