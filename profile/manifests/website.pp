# A website
define profile::website(
  Stdlib::AbsolutePath  $www_root               = "/var/www/${title}",
  Boolean               $ssl                    = true,
  Hash                  $add_header             = {},
  Optional[Hash]        $server_cfg_prepend     = undef,
  Optional[Hash]        $server_cfg_ssl_prepend = undef,
  Array                 $index_files            = [ 'index.htm', 'index.html' ],
  Array                 $rewrite_rules          = [],
  Boolean               $use_default_location   = true,
  Optional[Array]       $try_files              = undef,
  Stdlib::Absolutepath  $acme_webroot           = '/var/www/acme',
  Optional[Stdlib::Absolutepath] $ssl_cert      = undef,
  Optional[Stdlib::Absolutepath] $ssl_key       = undef,
  Boolean               $letsencrypt            = true,

) {

  case $ssl {
    true : {
      $_ssl_cert = $ssl_cert ? { undef => "/etc/letsencrypt/live/${title}/fullchain.pem", default => $ssl_cert }
      $_ssl_key = $ssl_key ? { undef => "/etc/letsencrypt/live/${title}/privkey.pem", default => $ssl_key }
    }
    default : {
      $_ssl_cert = undef
      $_ssl_key = undef
    }
  }

  nginx::resource::server { $title:
    www_root               => $www_root,
    ssl                    => $ssl,
    ssl_cert               => $_ssl_cert,
    ssl_key                => $_ssl_key,
    ssl_redirect           => $ssl,
    add_header             => $add_header,
    server_cfg_prepend     => $server_cfg_prepend,
    server_cfg_ssl_prepend => $server_cfg_ssl_prepend,
    index_files            => $index_files,
    rewrite_rules          => $rewrite_rules,
    try_files              => $try_files,
    use_default_location   => $use_default_location,
  }

  #
  # SSL/TLS
  #

  if $ssl and $letsencrypt {

    nginx::resource::location { "acme-webroot-${title}":
      server               => $title,
      location             => '^~ /.well-known/acme-challenge/',
      ssl                  => false,
      www_root             => $acme_webroot,
      try_files            => [ '$uri =404' ],
      index_files          => [],
      location_cfg_prepend => { 'default_type' => 'text/plain' },
      require              => File[$acme_webroot],
      notify               => Class['nginx::service'],
    }

    include profile::letsencrypt

    profile::letsencrypt::certonly { $title:
      domains      => [ $title ],
      plugin       => 'webroot',
      acme_webroot => $acme_webroot,
      require      => Nginx::Resource::Location["acme-webroot-${title}"],
    }

  }

}
