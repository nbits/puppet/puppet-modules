# A WordPress instance
define profile::wordpress::instance (
  String $db_pass,
  String $domain = $name,
  String $db_name = join(['wp_', regsubst($domain, /[^a-z0-9_]/, '_', 'G')]),
  String $charset = 'utf8mb3',
  String $collate = 'utf8mb3_general_ci',
  Optional[Stdlib::Absolutepath] $ssl_cert = undef,
  Optional[Stdlib::Absolutepath] $ssl_key  = undef,
  Boolean $letsencrypt                     = true,
) {
  if $domain !~ /[a-z0-9_.]+/ {
    fail("Invalid domain: ${domain}")
  }

  profile::mysql::db { $db_name:
    user    => $db_name,
    host    => 'localhost',
    pass    => $db_pass,
    charset => $charset,
    collate => $collate,
  }

  $php_socket = "/var/run/php-fpm/${db_name}"

  php::fpm::pool { $db_name:
    listen       => $php_socket,
    listen_owner => www-data,
    listen_group => www-data,
  }

  profile::website { $domain:
    www_root               => undef,
    add_header             => {
      'Strict-Transport-Security' => {'max-age=31536000; includeSubDomains; preload' => 'always'},
      'X-Content-Type-Options'    => {'nosniff' => 'always'},
      'Referrer-Policy'           => {'strict-origin-when-cross-origin' => 'always'},
      'X-Frame-Options'           => { 'SAMEORIGIN' => always },
    },
    server_cfg_ssl_prepend => {
      root                   => "/var/www/${domain}",
      fastcgi_buffers        => '16 16k',
      fastcgi_buffer_size    => '32k',
      server_tokens          => 'off',
    },
    index_files            => [ 'index.php' ],
    try_files              => [ '$uri', '$uri/', '/index.php?$args' ],
    ssl_cert               => $ssl_cert,
    ssl_key                => $ssl_key,
    letsencrypt            => $letsencrypt,
  }

  nginx::resource::location { "${domain} php":
    location             => '~ \.php$',
    server               => $domain,
    location_cfg_prepend => { 'root' => "/var/www/${domain}" },
    fastcgi              => "unix:${php_socket}",
    fastcgi_param        => { 'SCRIPT_FILENAME' => '$document_root$fastcgi_script_name' },
    location_cfg_append  => { 'fastcgi_intercept_errors'  => 'on' },
    ssl_only             => true,
  }

  nginx::resource::location { "${domain} static":
    location             => '~* \.(js|css|png|jpg|jpeg|gif|ico)$',
    server               => $domain,
    location_cfg_prepend => {
      'expires'       => 'max',
      'log_not_found' => 'off'
    },
    index_files          => [],
    ssl_only             => true,
  }
}
