# Install and manage Weechat
class profile::weechat {
  ensure_packages([
    'weechat',
    'python3-potr',
    'bitlbee',
  ])
  service {'bitlbee':
    ensure  => running,
    require => Package['bitlbee'],
  }
}
