# A monitoring profile
class profile::monitoring (
  Enum['agent', 'server'] $type = 'agent',
  Icinga2::CustomAttributes $vars = {},
) {

  include profile::monitoring::packages

  class { "profile::monitoring::${type}":
    vars => $vars,
  }

}
