# Manage needrestart
class profile::needrestart {
  ensure_packages([ 'needrestart' ])

  cron { 'needrestart restart auto':
    command => '/usr/sbin/needrestart -q -r a > /dev/null',
    minute  => 0,
    hour    => 3,
    user    => 'root',
  }
}
