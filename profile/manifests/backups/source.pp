# Configure a source for backups
class profile::backups::source (
  String $target = lookup('profile::backups::target', String, 'first', 'backups'),
  Stdlib::Absolutepath $prefix = lookup('profile::backups::target::prefix', Stdlib::Absolutepath, 'first'),
) {

  include borg

  $key_file = '/root/.ssh/id_ed25519_backups'

  ssh_keygen { 'root':
    type     => 'ed25519',
    filename => $key_file,
  }

  @@profile::backups::dir { "${prefix}/${trusted['certname']}":
    tag => $target,
  }

  ssh::client::config::user { 'root':
    ensure              => present,
    user_home_dir       => '/root',
    manage_user_ssh_dir => false,
    options             => {
      'Host backups.*' => {
        'Port'         => '2244',
        'User'         => 'backup',
        'IdentityFile' => $key_file,
      },
    },
  }

}
