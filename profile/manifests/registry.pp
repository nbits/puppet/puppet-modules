# A container registry that mirrors docker.io
class profile::registry (
  String $user = 'registry',
  String $group = 'registry',
  Integer $uid = 3000000,
  Integer $gid = 3000000,
  Stdlib::Absolutepath $home = '/var/lib/registry',
  String $container_publish = '5000:5000',
  Optional[String] $registry_proxy = undef,  # eg. 'https://registry-1.docker.io',
  Optional[Hash] $frontend = undef,
  Optional[String] $htpasswd_content = undef,
) {

  include profile::podman

  profile::podman::rootless { $user:
    group => $group,
    uid   => $uid,
    gid   => $gid,
    home  => $home,
  }

  $config_yaml = "${home}/config.yml"

  file { $config_yaml:
    ensure  => file,
    owner   => $user,
    group   => $group,
    mode    => '0644',
    content => epp('profile/registry/config.yml.epp', {
      proxy => $registry_proxy,
    }),
  }

  if $htpasswd_content {
    $htpasswd_path_host = "${home}/htpasswd"

    file { $htpasswd_path_host:
      ensure  => file,
      owner   => $user,
      group   => $group,
      mode    => '0640',
      content => $htpasswd_content,
      require => File[$home],
    }

    $env_file = "${home}/env"
    $htpasswd_path_container = '/etc/docker/registry/htpasswd'

    file { $env_file:
      ensure  => file,
      owner   => $user,
      group   => $group,
      mode    => '0644',
      content => epp('profile/registry/registry.env.epp', {
        htpasswd_path => $htpasswd_path_container,
      }),
      require => File[$home],
    }

    $htpasswd_flags = { env-file => $env_file }
    $htpasswd_volume = [ "${htpasswd_path_host}:${htpasswd_path_container}" ]
  }

  $container_flags = merge({
    publish => $container_publish,
    volume  => [
      'registry:/var/lib/registry',
      "${config_yaml}:/etc/docker/registry/config.yml",
    ] + ($htpasswd_volume ? { undef => [], default => $htpasswd_volume }),
  }, $htpasswd_flags)

  profile::podman::container { 'registry':
    image     => 'registry:2',
    flags     => $container_flags,
    user      => $user,
    subscribe => File[$config_yaml],
  }

  if $frontend {
    class { 'profile::registry::frontend':
      address => $frontend['address'],
      proxy   => $frontend['proxy'],
    }
  }

}
