# A GitLab runner instance
class profile::gitlab_runner (
  Integer $concurrent      = 10,
  Integer $check_interval  = 3,
  Integer $session_timeout = 1800,
  Hash $runners            = {},
) {

  apt::source { 'gitlab-runner':
    location => 'https://packages.gitlab.com/runner/gitlab-runner/debian',
    release  => 'buster',
    repos    => 'main',
    key      => {
      id     => 'F6403F6544A38863DAA0B6E03F01618A51312F3F',
      source => 'https://packages.gitlab.com/runner/gitlab-runner/gpgkey',
    },
  }

  ensure_packages([ 'gitlab-runner' ], { require => Apt::Source['gitlab-runner'] })


  ### Manage service using Systemd

  concat { '/etc/gitlab-runner/config.toml':
    owner   => gitlab-runner,
    group   => gitlab-runner,
    mode    => '0600',
    require => Package['gitlab-runner'],
  }

  concat::fragment { 'config.toml header':
    target  => '/etc/gitlab-runner/config.toml',
    content => epp('profile/gitlab_runner/config.toml.header.epp', {
      concurrent      => $concurrent,
      check_interval  => $check_interval,
      session_timeout => $session_timeout,
    }),
    order   => 1,
  }

  service {'gitlab-runner':
    ensure  => 'running',
  }

  ## Create runners from parameters

  ensure_packages([
    'libvirt-daemon',
    'libvirt-daemon-system',
    'virtinst',
    'libguestfs-tools',
  ])

  file { '/usr/local/lib/gitlab-runner':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  file { '/usr/local/lib/gitlab-runner/libvirt':
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => '0755',
    require => File['/usr/local/lib/gitlab-runner'],
  }

  $scripts = [ 'base', 'prepare', 'run', 'cleanup', 'create-base-image' ]
  $scripts.each | String $script | {
    file { "/usr/local/lib/gitlab-runner/libvirt/${script}.sh":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0755',
      source  => "puppet:///modules/profile/gitlab_runner/libvirt/${script}.sh",
      require => File['/usr/local/lib/gitlab-runner/libvirt'],
    }
  }

  ssh_keygen { 'root SSH key for GitLab Runner':
    user => 'root',
    type => 'rsa',
  }

  exec { 'Create Libvirt GitLab Runner base image':
    command => '/usr/local/lib/gitlab-runner/libvirt/create-base-image.sh',
    unless  => '/usr/bin/test -f /var/lib/libvirt/images/gitlab-runner-base.qcow2',
    user    => root,
    timeout => 1500,
    require => [
      Package['libvirt-daemon-system'],
      Package['libguestfs-tools'],
      Ssh_keygen['root SSH key for GitLab Runner'],
    ],
  }

  ensure_resources(profile::gitlab_runner::runner, $runners)

}
