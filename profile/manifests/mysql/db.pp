# A MySQL database
define profile::mysql::db (
  String $user = 'user',
  String $pass = 'password',
  String $host = $::fqdn,
  Array[String] $grant = ['ALL'],
  String $charset = 'utf8',
  String $collate = 'utf8_general_ci',
) {

  mysql::db { "${title}_${host}":
    user     => $user,
    password => $pass,
    dbname   => $title,
    host     => $host,
    grant    => $grant,
    charset  => $charset,
    collate  => $collate,
  }

}
