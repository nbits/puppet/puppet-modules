# Configure a MySQL server
class profile::mysql::server (
  String $root_password,
  String $backup_user,
  String $backup_password,
  String $backup_dir = '/var/backups/mysql',
  Enum['accept', 'drop'] $firewall_action = 'drop',
) {

  class { '::mysql::server':
    root_password           => $root_password,
    remove_default_accounts => true,
    restart                 => true,
    override_options        => {
      mysqld => {
        'bind-address' => '0.0.0.0',
        'ssl-ca'       => undef,
        'ssl-cert'     => undef,
        'ssl-key'      => undef,
      }
    }
  }

  class { 'mysql::server::backup':
    backupuser     => $backup_user,
    backuppassword => $backup_password,
    backupdir      => $backup_dir,
  }

  firewall { '100 accept TCP on MySQL':
    proto       => 'tcp',
    dport       => 3306,
    destination => $profile::network::address,
    action      => $firewall_action,
  }

}
