# Configure the system to send e-mail
class profile::mail (
  Optional[Hash]                  $configs             = {},
  String                          $inet_interfaces     = 'all',
  String                          $inet_protocols      = 'all',
  String                          $mynetworks          = '127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128', # postfix_mynetworks
  String                          $myorigin            = $::fqdn,       # postfix_myorigin
  Boolean                         $opendkim            = false,
  Optional[String]                $relayhost           = undef,
  Variant[Array[String], String]  $root_mail_recipient = 'nobody',
  Boolean                         $satellite           = false,
  String                          $smtp_listen         = '127.0.0.1',   # postfix_smtp_listen
  Variant[Integer[2, 3], Boolean] $use_schleuder       = false,         # postfix_use_schleuder
) {

  ensure_packages([ 'augeas-tools', 'postfix-pcre' ])

  Package['postfix'] -> Package['postfix-pcre']

  class { 'augeas':
    lens_dir => '/usr/share/augeas/lenses',
    # The catalog build fails in the augeas module's versioncmp if the
    # following package is not installed
    require  => Package['augeas-tools'],
  }

  class { 'postfix':
    configs             => $configs,
    inet_interfaces     => $inet_interfaces,
    inet_protocols      => $inet_protocols,
    mynetworks          => $mynetworks,
    myorigin            => $myorigin,
    relayhost           => $relayhost,
    root_mail_recipient => $root_mail_recipient,
    satellite           => $satellite,
    smtp_listen         => $smtp_listen,
    use_schleuder       => $use_schleuder,
  }

  if $opendkim {
    include opendkim

    postfix::config { 'smtpd_milters':
      value => 'unix:/var/run/opendkim/opendkim.sock',
    }

    postfix::config { 'non_smtpd_milters':
      value => '$smtpd_milters',
    }
  }

}
