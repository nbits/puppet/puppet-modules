# A Podman-based ONLYOFFICE instance
define profile::onlyoffice::instance (
  Integer $uid,
  Integer $gid,
  String $jwt_secret,
  Integer $local_port = 8000,
  Optional[Integer] $public_port = undef,
  String $public_address = "onlyoffice.${::fqdn}",
  String $onlyoffice_image = 'docker.io/alehoho/oo-ce-docker-license:6.3.1.32',
  String $redis_image = 'docker.io/redis:5.0-alpine',
  String $rabbitmq_image = 'docker.io/rabbitmq:3.8-alpine',
  String $postgresql_image = 'docker.io/postgres:12-alpine',
  Boolean $generate_fonts = false,
  Integer $nginx_worker_processes = 1,
  Integer $nginx_worker_connections = 512,
  Boolean $ssl = true,
) {
  include profile::podman

  $identifier = "onlyoffice_${name}"

  # Downloading containers may take long.
  Exec {
    timeout => 1800,
  }

  ## Podman

  $home_dir = "/var/lib/onlyoffice/${name}"
  $podman_user = $identifier
  $podman_group = $identifier

  profile::podman::rootless { $podman_user:
    group   => $podman_group,
    uid     => $uid,
    gid     => $gid,
    home    => $home_dir,
    require => File['/var/lib/onlyoffice'],
  }

  $pod_network = $facts['os']['distro']['codename'] ? {
    'bullseye' => 'private',
    default    => 'slirp4netns',
  }

  podman::pod { $identifier:
    user  => $podman_user,
    flags => {
      network => $pod_network,
      publish => "${local_port}:8000",
    },
  }

  $onlyoffice_container_name = "onlyoffice_${name}_onlyoffice"
  $redis_container_name = "onlyoffice_${name}_redis"
  $rabbitmq_container_name = "onlyoffice_${name}_rabbitmq"
  $postgresql_container_name = "onlyoffice_${name}_postgresql"

  ## Redis container

  $redis_data_dir = "${home_dir}/redis"

  file { $redis_data_dir:
    ensure  => directory,
    owner   => $uid + 999,
    group   => $podman_group,
    mode    => '0750',
    require => File[$home_dir],
  }

  profile::podman::container { $redis_container_name:
    image   => $redis_image,
    user    => $podman_user,
    command => 'redis-server --appendonly yes',
    flags   => {
      pod    => $identifier,
      volume => [ "${redis_data_dir}:/data:Z" ],
    },
    require => [
      File[$redis_data_dir],
      Podman::Pod[$identifier],
    ],
  }

  ## RabbitMQ container

  profile::podman::container { $rabbitmq_container_name:
    image   => $rabbitmq_image,
    user    => $podman_user,
    flags   => {
      pod      => $identifier,
    },
    require => [
      Podman::Pod[$identifier],
    ],
  }

  ## PostgreSQL container

  $postgresql_data_dir = "${home_dir}/postgresql"

  file { $postgresql_data_dir:
    ensure  => directory,
    owner   => $uid + 999,
    group   => $podman_group,
    mode    => '0750',
    require => File[$home_dir],
  }

  profile::podman::container { $postgresql_container_name:
    image   => $postgresql_image,
    user    => $podman_user,
    flags   => {
      pod    => $identifier,
      volume => [
        "${postgresql_data_dir}:/var/lib/postgresq/datal:Z",
      ],
      env    => [
        'POSTGRES_DB=onlyoffice',
        'POSTGRES_USER=onlyoffice',
        'POSTGRES_PASSWORD=onlyoffice',
      ],
    },
    require => [
      Podman::Pod[$identifier],
      File[$postgresql_data_dir],
    ],
  }

  ## ONLYOFFICE container

  $env_file = "${home_dir}/container.env"

  file { $env_file:
    owner   => $podman_user,
    group   => $podman_group,
    mode    => '0600',
    content => epp('profile/onlyoffice/container.env.epp', {
      amqp_uri                 => 'amqp://guest:guest@127.0.0.1:5672',
      generate_fonts           => $generate_fonts,
      jwt_secret               => $jwt_secret,
      nginx_worker_processes   => $nginx_worker_processes,
      nginx_worker_connections => $nginx_worker_connections,
      postgresql_host          => '127.0.0.1',
      postgresql_database      => 'onlyoffice',
      postgresql_user          => 'onlyoffice',
      postgresql_password      => 'onlyoffice',
      redis_server_host        => '127.0.0.1',
    }),
  }

  $logs_dir = "${home_dir}/logs"
  $data_dir = "${home_dir}/data"

  file { [$logs_dir, $data_dir]:
    ensure => directory,
    owner  => $uid + 109,
    group  => $gid + 111,
    mode   => '0644',
  }

  profile::podman::container { $onlyoffice_container_name:
    image   => $onlyoffice_image,
    user    => $podman_user,
    flags   => {
      pod      => $identifier,
      env-file => $env_file,
      volume   => [
        "${logs_dir}:/var/log/onlyoffice:Z",
        "${data_dir}:/var/www/onlyoffice/Data:Z",
      ],
    },
    require => [
      File[$env_file],
      File[$logs_dir],
      File[$data_dir],
      Podman::Pod[$identifier],
    ],
  }

  # Optional public access

  if $public_port {
    profile::onlyoffice::frontend { $public_address:
      public_port => $public_port,
      local_port  => $local_port,
      ssl         => $ssl,
    }
  }

}
