# A frontend for public container registries
class profile::registry::frontend (
  String $address = "registry.${::fqdn}",
  String $proxy = '127.0.0.1:5000',
) {

  include profile::nginx
  include profile::letsencrypt
  include profile::firewall::web

  profile::reverse_proxy { $address:
    proxy => $proxy,
  }

}
