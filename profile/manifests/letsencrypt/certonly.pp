# Let's Encrypt certonly
define profile::letsencrypt::certonly (
  Stdlib::Absolutepath $acme_webroot = $profile::letsencrypt::acme_webroot,
  String $plugin = 'webroot',
  Array[String] $domains = [ $title ],
  Stdlib::Absolutepath $tls_root = '/var/local/tls',
  Array[String] $deploy_hook_commands = [],
) {

  letsencrypt::certonly { $title:
    domains              => $domains,
    plugin               => $plugin,
    webroot_paths        => [ $acme_webroot ],
    deploy_hook_commands => $deploy_hook_commands,
    require              => [
      Service['nginx'],
    ],
    notify               => Exec["Create TLS links for ${title} (post-nginx)"],
  }

  $letsencrypt_cert = "/etc/letsencrypt/live/${title}/fullchain.pem"
  $cert_link = "${tls_root}/${title}/fullchain.pem"

  # Nginx needs TLS certs to startup, but generation of valid TLS certs depend
  # on Nginx to serve the ACME challenge. We workaround this circular
  # dependency by using a script to configure links in `$tls_root` to either
  # a snakeoil cert or else to valid TLS certs when they are available.
  exec { "Create TLS links for ${title} (pre-nginx)":
    command => "/usr/local/sbin/create-tls-links.sh ${title} ${tls_root}",
    unless  => "/usr/bin/test \"$( readlink ${cert_link} )\" = \"${letsencrypt_cert}\"",
  }
  -> Service['nginx']
  # If a certificate was just generated, we want to make sure that links point
  # to the new cert/key, so we run the script once again. In this case the
  # script also reloads Nginx, as we'd create a circular dependency if we
  # notified Service['nginx'] in the below exec.
  -> exec { "Create TLS links for ${title} (post-nginx)":
    command     => "/usr/local/sbin/create-tls-links.sh ${title} ${tls_root} reload",
    unless      => "/usr/bin/test \"$( readlink ${cert_link} )\" = \"${letsencrypt_cert}\"",
    refreshonly => true,
  }

}
