# Manage molly_guard
class profile::molly_guard {
  class { 'molly_guard':
    always_query_hostname => true,
  }
}
