# A WordPress-capable server
class profile::wordpress (
  Hash $instances = {},
) {
  include php
  include profile::nginx
  include profile::mysql::server

  ensure_packages([
    'php-mysql',
    'php-zip',
    'php-mbstring',
    'php-gd',
    'php-curl',
  ], { ensure => latest })

  ensure_resources('profile::wordpress::instance', $instances)
}
