# SSH service
class profile::sshd (
  Integer $ssh_port          = 22,
  Boolean $permit_root_login = false,
  Hash $options              = {},
) {

  firewall { '100 allow SSH access':
    dport  => String($ssh_port),
    proto  => 'tcp',
    action => 'accept',
  }

  $permit_root_login_str = $permit_root_login ? { true => 'yes', default => 'no' }

  $default_options = {
    'PermitRootLogin'        => $permit_root_login_str,
    'Port'                   => [ String($ssh_port) ],
    'X11Forwarding'          => 'no',
  }

  class { 'ssh::server':
    options => deep_merge($default_options, $options),
  }

  include tor

  tor::daemon::onion_service { 'ssh':
    ports => [ String($ssh_port) ],
  }

}
