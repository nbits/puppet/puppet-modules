# Firewall configurations for Tinc
class profile::tinc::firewall (
  String $netname = 'tun0',
  Enum['client', 'server'] $type = 'client',
  String $client_ip = $facts['networking']['ip'],
) {

  $tag_netname = "netname:${netname}"
  $tag_environment = "environment:${::environment}"

  if $type == 'server' {

    Firewall <<| tag == 'tinc_client_whitelist' and tag == $tag_netname and tag == $tag_environment |>>

  } else {

    ['tcp', 'udp'].each | String $proto | {
      @@firewall { "100 accept ${proto.upcase()} on Tinc port from ${trusted['certname']}":
        dport  => [ 655 ],
        proto  => $proto,
        action => 'accept',
        source => "${client_ip}/32",
        tag    => [ 'tinc_client_whitelist', $tag_netname, $tag_environment ],
      }
    }

  }

}
