# Configure tinc hosts
class profile::tinc::hosts (
  String $tinc_name = regsubst($trusted['certname'], '[^[a-zA-Z0-9]]', '_', 'G'),
  String $address = $facts['networking']['ip'],
  String $netname = 'tun0',
  Stdlib::Absolutepath $config_dir = "/etc/tinc/${netname}",
  Optional[String] $ip = undef,
) {

  #
  # Create a keypair for this host
  #

  $privkey_file = "${config_dir}/rsa_key.priv"
  $pubkey_file = "${config_dir}/rsa_key.pub"

  exec { 'generate tinc keys':
    command => "/usr/sbin/tincd --config ${config_dir} --generate-keys < /dev/null",
    creates => $privkey_file,
  }

  file { $privkey_file:
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    require => Exec['generate tinc keys'],
  }

  file { $pubkey_file:
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    require => Exec['generate tinc keys'],
  }

  #
  # Create config files for all hosts
  #

  $host_file = "${config_dir}/hosts/${tinc_name}"
  $pubkey = fact('tinc_pubkey')
  $tag_netname = "netname:${netname}"
  $tag_environment = "environment:${::environment}"

  @@file { $host_file:
    ensure  => present,
    mode    => '0600',
    owner   => root,
    group   => root,
    content => epp('profile/tinc/host.epp', {
      address => $address,
      ip      => $ip,
      pubkey  => $pubkey,
    }),
    tag     => [ 'tinc_host_file', $tag_netname, $tag_environment ],
  }

  File <<| tag == 'tinc_host_file' and tag == $tag_netname and tag == $tag_environment |>>

}
