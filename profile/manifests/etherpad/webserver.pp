class profile::etherpad::webserver {
  include profile::nginx

  $files = ['index.html', 'bandeirinha.png', 'fundo.png', 'favicon.ico']

  $files.each |$file| {
    file { "/var/lib/etherpad/${file}":
      ensure => file,
      owner  => 'etherpad',
      group  => 'etherpad',
      mode   => '0644',
      source => "puppet:///modules/profile/etherpad/${file}",
    }
  }

  nginx::resource::server { 'etherpad-lite':
    server_name          => [ '127.0.0.1' ],
    listen_port          => 80,
    use_default_location => false,
    index_files          => ['index.html'],
    www_root             => '/var/lib/etherpad',
  }

  nginx::resource::map { 'connection_upgrade':
    default  => 'upgrade',
    string   => '$http_upgrade',
    mappings => {'\'\'' => 'close'},
  }

  nginx::resource::location { 'etherpad-lite':
    server             => 'etherpad-lite',
    location           => '/',
    proxy              => 'http://127.0.0.1:9001/',
    proxy_buffering    => 'off',
    proxy_http_version => '1.1',
    proxy_pass_header  => ['Server'],
    proxy_set_header   => [
      'Host $host',
      'X-Real-IP $remote_addr',
      'X-Forwarded-For $proxy_add_x_forwarded_for',
      'X-Forwarded-Host $host',
      'X-Forwarded-Proto $scheme',
      'Upgrade $http_upgrade',
      'Connection $connection_upgrade',
    ],
  }

  nginx::resource::location { 'etherpad-lite-index':
    server      => 'etherpad-lite',
    location    => '~ ^/$',
    index_files => [],
    try_files   => ['/index.html', '=404'],
  }

  nginx::resource::location { 'etherpad-lite-bandeirinha':
    server      => 'etherpad-lite',
    location    => '/bandeirinha.png',
    index_files => [],
    try_files   => ['$uri', '=404'],
  }

  nginx::resource::location { 'etherpad-lite-fundo':
    server      => 'etherpad-lite',
    location    => '/fundo.png',
    index_files => [],
    try_files   => ['$uri', '=404'],
  }

  nginx::resource::location { 'etherpad-lite-favicon':
    server      => 'etherpad-lite',
    location    => '/favicon.ico',
    index_files => [],
    try_files   => ['$uri', '=404'],
  }

  nginx::resource::location { 'etherpad-lite-admin':
    server              => 'etherpad-lite',
    location            => '/admin',
    index_files         => [],
    location_cfg_append => {'return' => '401'},
  }
}
