class profile::etherpad::container (
  String $admin_password,
  String $db_password,
) {
  include profile::podman

  profile::podman::rootless { 'etherpad': }

  podman::pod { 'etherpad':
    user => 'etherpad',
    flags => {
      network => 'slirp4netns',
      publish => '9001:9001',
    },
  }

  file { '/var/lib/etherpad/settings.json':
    ensure => file,
    owner  => 'etherpad',
    group  => 'etherpad',
    mode   => '0644',
    source => 'puppet:///modules/profile/etherpad/settings.json',
    notify => Profile::Podman::Container['etherpad-lite'],
  }

  profile::podman::container { 'etherpad-lite':
    image => 'docker.io/etherpad/etherpad:latest',
    user => 'etherpad',
    flags => {
      pod    => 'etherpad',
      volume => [
        'plugins:/opt/etherpad-lite/src/plugin_packages:z',
        'etherpad-var:/opt/etherpad-lite/var:z',
        '/var/lib/etherpad/settings.json:/opt/etherpad-lite/settings.json',
      ],
      env    => [
        'NODE_ENV=production',
        "ADMIN_PASSWORD=${admin_password}",
        'DB_CHARSET=utf8mb4',
        'DB_HOST=mariadb',
        'DB_NAME=etherpad',
        "DB_PASS=${db_password}",
        'DB_PORT=3306',
        'DB_TYPE=mysql',
        'DB_USER=etherpad',
        'DEFAULT_PAD_TEXT=Este pad será apagado automaticamente após 60 dias sem edição.',
        'DISABLE_IP_LOGGING=true',
        'PAD_OPTIONS_USE_MONOSPACE_FONT=true',
        'SOFFICE=null',
        'TRUST_PROXY=true',
      ],
      label  => [
        'io.containers.autoupdate=registry',
        'PODMAN_SYSTEMD_UNIT=podman-etherpad-lite.service',
      ],
    },
    require => [
      Podman::Pod['etherpad'],
      Podman::Container['mariadb'],
      File['/var/lib/etherpad/settings.json'],
    ],
  }

  profile::podman::container { 'mariadb':
    image   => 'docker.io/mariadb:lts',
    user    => 'etherpad',
    flags   => {
      pod     => 'etherpad',
      env     => [
        'MARIADB_RANDOM_ROOT_PASSWORD=true',
        'MARIADB_USER=etherpad',
        "MARIADB_PASSWORD=${db_password}",
        'MARIADB_DATABASE=etherpad',
      ],
      volume => [
        'mariadb_data:/var/lib/mysql',
      ],
      label  => [
        'io.containers.autoupdate=registry',
        'PODMAN_SYSTEMD_UNIT=podman-mariadb.service',
      ],
    },
    require => Podman::Pod['etherpad'],
  }
}
