#!/bin/sh

# Get latest tag from GitHub API

for module in $( git submodule status | awk '{ print $2 }' ); do
	url=$( git -C ${module} remote get-url origin )
	# website=$( echo ${url} | sed -e 's/https:\/\/\([^\/]\+\).*$/\1/' )
	project=$( echo ${url} | sed -e 's/https:\/\/[^/\]\+\/\([^.]*\)\(\.git\)\?$/\1/' )
	if [ "${project}" = "ninfra/puppet-puppet" ]; then
		continue
	fi
	endpoint="https://api.github.com/repos/${project}/tags"
	latest=$( curl -s $endpoint | jq -r '.[0]["name"]' )
	echo "Updating ${project} to ${latest}"
	git -C ${module} fetch
	git -C ${module} checkout ${latest}
done
